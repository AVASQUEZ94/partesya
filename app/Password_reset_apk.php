<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Password_reset_apk extends Model {

	//
	protected $table="password_reset_apk";
	public $timestamps = false;
}