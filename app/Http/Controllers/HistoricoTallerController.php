<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

//use App\Http\Requests\TipoTallerRequest;
//use App\Http\Requests\UpdateTipoTallerRequest;
use App\Taller;
use App\User;
use Redirect;
use Gmaps;
use App\TallerServicio;
use App\Servicio;
use App\TipoTaller;

use App\HistoricoTaller;
use App\HistoricoSesion;
use App\HistoricoUsuario;



class HistoricoTallerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}



	public function historico_taller(Request $request){

		#c >orderBy("created_at","desc")-
		$view="HISTORICO TALLER";
      			$title="HISTORICO";
		$fechas=HistoricoTaller::select(\DB::raw("DISTINCT cast(created_at as date) fecha_dia, DATE_FORMAT((created_at - INTERVAL 5 HOUR),'%W, %d de %M del %Y') as fecha_todas"))
					->orderBy("created_at",$request->order)->get();

		return view ("historico.taller")->with(compact("fechas","view","title"));
		
	}

	public function historico_sesion(Request $request){

		#c >orderBy("created_at","desc")-
			$view="HISTORICO SESIÓN";
      			$title="HISTORICO";
		$fechas=HistoricoSesion::select(\DB::raw("DISTINCT cast(created_at as date) fecha_dia, DATE_FORMAT((created_at - INTERVAL 5 HOUR),'%W, %d de %M del %Y') as fecha_todas"))
					->orderBy("created_at",$request->order)->get();

		return view ("historico.sesion")->with(compact("fechas","view","title"));
		
	}

	public function historico_usuario(Request $request){

		#c >orderBy("created_at","desc")-
	$view="HISTORICO USUARIOS";
      			$title="HISTORICO";
		$fechas=HistoricoUsuario::select(\DB::raw("DISTINCT cast(created_at as date) fecha_dia, DATE_FORMAT((created_at - INTERVAL 5 HOUR),'%W, %d de %M del %Y') as fecha_todas"))
					->orderBy("created_at",$request->order)->get();

		return view ("historico.usuario")->with(compact("fechas","view","title"));
		
	}






}
