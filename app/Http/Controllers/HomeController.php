<?php namespace App\Http\Controllers;

use App\User;
use App\Taller;
use App\Productos;
use App\Almacen;
class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	
	
	public function index()
	{


		 if (Auth::check()){
  		  $view="Inicio";
   		   $title="Inicio";
			return view('menu.indexes.list')->with(compact("view","title"));

			}
			else{
				return view("index")->with(compact("view","title"));
			}

	}


		public function totalusuarios(){

		
		 	echo json_encode(User::count('id'));

		}

		public function totalproductos(){

		
		 	echo json_encode(Productos::count('id'));

		}

		public function totaltaller(){

		
		 	echo json_encode(Taller::count('id'));

		}

		public function totalalmacen(){

		
		 	echo json_encode(Almacen::count('id_almacen'));

		}


		public function almacenesapi(){

		
		 	echo json_encode(Almacen::get());

		}



}
