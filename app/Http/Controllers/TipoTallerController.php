<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests\TipoTallerRequest;
use App\Http\Requests\UpdateTipoTallerRequest;
use App\Taller;
use App\User;
use Redirect;
use Gmaps;
use App\TallerServicio;
use App\Servicio;
use App\TipoTaller;

class TipoTallerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$registros=TipoTaller::orderby("descripcion","asc")->get();

	  $view="LISTA DE TIPOS DE TALLERES";
      $title="TIPO DE TALLER";

		return view("tipotaller.index")->with(compact("registros","view","title"));


	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	  $view="CREAR TIPO DE TALLER";
      $title="TIPO DE TALLER";

		return view("tipotaller.gestion")->with(compact("view","title"));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(TipoTallerRequest $request)
	{
		if(TipoTaller::whereRaw("UPPER(descripcion) = '".strtoupper($request->descripcion)."' ")->count("id")==0){
			$nuevo=new TipoTaller;
			$nuevo->descripcion=$request->descripcion;

			if($nuevo->save()){
				$view="LISTA DE TIPOS DE TALLERES";
      			$title="TIPO DE TALLER";
				return Redirect::to("/tipo_taller")->with('message', 'Registro Agregado correctamente!',compact("view","title"));

			}else{
				$view="CREAR TALLER";
      			$title="TIPO DE TALLER";
				return Redirect::back()->with('alert', 'Error al agregar el resgitro.!',compat("view","title"));
			}
		}else{

				$view="LISTA DE TIPOS DE TALLERES";
      			$title="TIPO DE TALLER";

				return Redirect::back()->with('alert', 'Error ya se encuentra un registro con este nombre! '.$request->descripcion.'',compat("view","title"));


			}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$doc=TipoTaller::find($id);
		$view="EDITAR TIPOS DE TALLERES";
      	$title="TIPO DE TALLER";
		return view("tipotaller.modificar")->with(compact("doc","view","title"));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(UpdateTipoTallerRequest $request,$id)
	{
		if(TipoTaller::whereRaw("UPPER(descripcion) = '".strtoupper($request->descripcion)."' ")->where("id","!=", $id)->count("id")==0){
			$doc=TipoTaller::find($id);
			$doc->descripcion=$request->descripcion;			
						
			if($doc->save()){
				$view="LISTA DE TIPOS DE TALLERES";
      			$title="TIPO DE TALLER";
				return Redirect::to("/tipo_taller")->with('message', 'Registro Modificado correctamente!',compact("view","title"));

			}else{
				$view="EDITAR TIPOS DE TALLERES";
      			$title="TIPO DE TALLER";

				return Redirect::back()->with('alert', 'Error al modificar el resgitro.!',compact("view","title"));
			}
		}else{
			$view="EDITAR TIPOS DE TALLERES";
      		$title="TIPO DE TALLER";
			
			return Redirect::back()->with('alert', 'Error ya se encuentra un registro con este nombre! '.$request->descripcion.'',compact("view","title"));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		if(tipotaller::count("id")==1){
			return Redirect::back()->with('alert', 'Error, Debe dejar almenos un registro en la base de datos.');
				}

		$doc=TipoTaller::destroy($id);
			if($doc){
					$view="LISTA TIPOS DE TALLERES";
      		$title="TIPO DE TALLER";
			return Redirect::to("/tipo_taller")->with('message', 'Registro Eliminado correctamente!',compact("view","title"));

		}else{
				$view="EDITAR TIPOS DE TALLERES";
      		$title="TIPO DE TALLER";
			return Redirect::back()->with('alert', 'Error al eliminar el resgitro.!',compact("view","title"));
		}
	}

}
