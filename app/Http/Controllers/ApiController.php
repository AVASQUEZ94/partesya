<?php 
namespace App\Http\Controllers; 

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;

use Illuminate\Http\Request;
use App\User;
use App\Taller;
use App\Productos;
use App\Almacen;
use App\Almacen_ranking;
use App\Taller_ranking;
use App\Password_reset_apk;
use App\Motores;
use App\Categorias;
use App\Subcategorias;
use App\Orders_cart;
use App\Order_items_cart;
use Auth;

use Socialite;
use Hash;
use Config;
use Validator;
use Firebase\JWT\JWT;
use GuzzleHttp;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use App\Compras;
use Mail;
use DB;

class ApiController extends Controller {

		/*
		Metodos para manejar el carrito de compras
		*/
		
		public function addCart(Request $datos){

       
		$user_cart = Orders_cart::where('user_id', '=', $datos->user_id)->first();
		if(isset($user_cart) && $user_cart->id > 0){
		   $subtotal = $user_cart->subtotal + $datos->precio;   
		   $user_cart->subtotal = $subtotal;
		   $user_cart->save();
		
	       $itemsCart = Order_items_cart::where('product_id', '=', $datos->product_id)
		   ->where('order_id','=',$user_cart->id)
		   ->first();
			if(isset($itemsCart) && $itemsCart->id > 0)	{
				$itemsCart->quantity = $itemsCart->quantity + $datos->quantity;
			}else{
				$itemsCart = new Order_items_cart;
				$itemsCart->quantity = $datos->quantity;
				$itemsCart->price = $datos->precio;
				$itemsCart->product_id = $datos->product_id;
				$itemsCart->order_id = $user_cart->id;	
			}
		    	
			$itemsCart->save();
            $array=Array( "result"=> "Actualizacion de carrito", "codError" =>'200');
			echo json_encode($array);
           

		}else{
                $user_cart = new Orders_cart;
				$user_cart->subtotal = $datos->precio;
				$user_cart->shipping = 0;
				$user_cart->user_id = $datos->user_id;	
				$user_cart->address_send = '';
				$user_cart->save();
				$idOrder = DB::table('orders_cart')->where('user_id', '=', $datos->user_id)->max('id');

				$itemsCart = new Order_items_cart;
				$itemsCart->quantity = $datos->quantity;
				$itemsCart->price = $datos->precio;
				$itemsCart->product_id = $datos->product_id;
				$itemsCart->order_id = $idOrder;
				$itemsCart->save();				  
				$array=Array( "result"=> "Se ha agregado el producto al carrito", "codError" =>'200');
				echo json_encode($array);

		}

	}
	
	//-----------------------------------------------------------------------------------------------------------------------------------------
	public function getCart(Request $datos){

			$user_cart = Orders_cart::where('user_id', '=', $datos->user_id)->first();
		    if(isset($user_cart) && $user_cart->id > 0){

					$itemsCart =  Productos::select(\DB::raw('producto.id as idP, producto.* , order_items_cart.id as idI, order_items_cart.* '))
							->join('order_items_cart','order_items_cart.product_id','=','producto.id')
							->where('order_id','=',$user_cart->id)
							->distinct()
		 					->orderBY('producto.nombre','desc')
		 					->get();
				
				    $cartBadge = Order_items_cart::select(\DB::raw('sum(quantity) as cantidad '))
							->where('order_id','=',$user_cart->id)
		 					->get();
										
					$array=Array( "result"=> "Listado de productos del carrito", "codError" =>'200',"order" => $user_cart,"orderItems" => $itemsCart, "cartBadge" => $cartBadge);
					echo json_encode($array); 
	
			}else{
			
					
					$array=Array( "result"=> "No se han agregado productos al carrito", "codError" =>'201');
					echo json_encode($array);
			}
	
		}	
	//-----------------------------------------------------------------------------------------------------------------------------------------	
	public function getCartBadge(Request $datos){
			
			$user_cart = Orders_cart::where('user_id', '=', $datos->user_id)->first();

		    if(isset($user_cart) && $user_cart->id > 0){

					   
					$itemsCart = Order_items_cart::select(\DB::raw('sum(quantity) as cantidad '))
							->where('order_id','=',$user_cart->id)
		 					->get();
	
					$array=Array( "result"=> "Listado de productos del carrito", "codError" =>'200',"cartBadge" => $itemsCart);
					echo json_encode($array); 
	
			}else{
			
					
					$array=Array( "result"=> "No se han agregado productos al carrito", "codError" =>'201');
					echo json_encode($array);
			}
	}
	//-------------------------------------------------------------------------------------------------------------------------------------------
	public function getBadgeCart(Request $datos){
	
 		    $user_cart = Orders_cart::where('user_id', '=', $datos->user_id)->first();

			
		    if(isset($user_cart) && $user_cart->id > 0){

					$itemsCart =  Productos::select(\DB::raw('producto.* , order_items_cart.*'))
							->join('order_items_cart','order_items_cart.product_id','=','producto.id')
							->distinct()
		 					->orderBY('producto.nombre','desc')
		 					->get();
							
					$array=Array( "result"=> "Listado de productos del carrito", "codError" =>'200',"order" => $user_cart,"orderItems" => $itemsCart);
					echo json_encode($array); 
					
	
			}else{
			
					
					$array=Array( "result"=> "No se han agregado productos al carrito", "codError" =>'201');
					echo json_encode($array);
			}
	
		}	
		//--------------------------------------------------------------------------------------------------------------------------------------
		public function delItemCart(Request $datos){
		
			$user_cart = Orders_cart::where('user_id', '=', $datos->user_id)->first();
			$msj = "";
			if(isset($user_cart) && $user_cart->id > 0){
				if($datos->accion=='vaciar'){
					Order_items_cart::where('order_id','=',$user_cart->id)->delete();
					Orders_cart::where('user_id', '=', $datos->user_id)->delete();
					$array=Array( "result"=> "Han sido eliminado los productos del carrito", "codError" =>'200',"items"=>NULL);
					echo json_encode($array);
				}else{
					$itemsCart = Order_items_cart::where('product_id', '=', $datos->product_id)
					   ->where('order_id','=',$user_cart->id)
					   ->first();
					 
					if(isset($itemsCart) && $itemsCart->id > 0)	{
						  if($datos->accion=='subtract'){
							if($itemsCart->quantity>1 && $datos->quantity>=1){
								$itemsCart->quantity = $itemsCart->quantity - 1;
								$itemsCart->save();
								$msj = "Se actualizo la cantidad";
							}else{
								$itemsCart->delete(); // si la cantidad es 1 se elimina el producto del carrito
								$msj = "Se elimino el producto ";
								$itemsCart = Order_items_cart::where('order_id','=',$user_cart->id)
											->first();
								if(!$itemsCart){
									$user_cart->delete(); //Si no hay productos en el carro se elimina esa orden	
									$msj.= "- Se elimino la orden";
								} 
							 }
							}	
							else {
							 $itemsCart->quantity = $itemsCart->quantity + 1;
							 $itemsCart->save();
							 $msj = "Se actualizo la cantidad";
							} 	
						/*$array=Array( "result"=> $msj, "codError" =>'200');
						echo json_encode($array); */
						$this->getCart($datos);
								
					}else{
						$array=Array( "result"=> "No existe el producto en el carrito", "codError" =>'200',"items"=>$itemsCart,"datos"=>$datos->product_id.'--'.$datos->user_id);
						echo json_encode($array);
					}	
			}		
			}else{
					$array=Array( "result"=> "No exite ninguna orden asociada al usuario", "codError" =>'201');
					echo json_encode($array);
			}
		}
		
		//--------------------------------------------------------------------------------------------------------------------------------------
		
		/*busca los almacenes y los ordena ascendente*/

		public function almacenes(Request $request){


			if($request->nombre!=''){
		
			echo json_encode(

				Almacen::where('status','=',1)
						->where('nombre','like',"%%".$request->nombre."%%")
						->orderBY('nombre','ASC')
						->limit(1)
						->get()

							);

			}
			else{

			echo json_encode(
				Almacen::where('status','=',1)
						->orderBY('nombre','ASC')
						->limit(150)
						->get()

						);

			}

		
		}

	//--------------------------------------------------------------------------------------------------------------------------------------
		
		/*busca los talleres y los ordena ascendente*/

		public function talleres(Request $request){


			if($request->nombre!=''){
		
			echo json_encode(

				Taller::where('status','=',1)
						->where('nombre','like',"%%".$request->nombre."%%")
						->orderBY('nombre','ASC')
						->limit(1)
						->get()

							);

			}
			else{

			echo json_encode(
				Taller::where('status','=',1)
						->orderBY('nombre','ASC')
						->limit(150)
						->get()
						);

			}

		
		}
		
		/*busca los servicios de un taller*/

		public function servTalleres(Request $request){


			if($request->idTaller!=''){
		
			echo json_encode(

				\DB::table('taller_servicios')->select()
											->where('id_taller', '=', $request->idTaller)
											->orderBY('descripcion','ASC')					
											->get()
							);

			}
		
		}
		
		/*busca las ofertas en el sistema*/
		public function ofertas(){

		
		 	echo json_encode(

		 	Productos::select(\DB::raw('producto.* , producto.año as anio'))
							->join('almacen_producto','almacen_producto.id_producto','=','producto.id')
		 					->join('almacen','almacen_producto.id_almacen','=','almacen.id_almacen')
		 					->where('almacen.status','=',1)
		 					->orderBY('producto.nombre','desc')
		 					->limit(150)
		 					->get()

		 					);

		}



		/*busca  los productos registrados, y se filtra por nombre. boton autopartes*/
		public function autopartes(Request $request){

			if($request->nombre!=''){
		
		
			echo json_encode(

						Productos::select(\DB::raw('producto.* , producto.año as anio'))
			 					->where('producto.nombre','like',"%%".$request->nombre."%%")
			 					->orderBY('producto.nombre','desc')
			 					->limit(150)
			 					->get()
							);
					/*Con almacenes
					Productos::select(\DB::raw('producto.* , producto.año as anio, almacen_producto.*'))
								->join('almacen_producto','almacen_producto.id_producto','=','producto.id')
			 					->join('almacen','almacen_producto.id_almacen','=','almacen.id_almacen')
			 					->where('almacen.status','=',1)
			 					->where('producto.nombre','like',"%%".$request->nombre."%%")
			 					->orderBY('producto.nombre','desc')
			 					->limit(150)
			 					->get()
							);*/

			}
			else{

				echo json_encode(

					/*Con almacenes
					Productos::select(\DB::raw('producto.* , producto.año as anio, almacen_producto.*'))
								->join('almacen_producto','almacen_producto.id_producto','=','producto.id')
			 					->join('almacen','almacen_producto.id_almacen','=','almacen.id_almacen')
			 					->where('almacen.status','=',1)
			 					->orderBY('producto.nombre','desc')
			 					->limit(150)
			 					->get()

		 						);
					*/
					Productos::select(\DB::raw('producto.* , producto.año as anio'))
			 					->orderBY('producto.nombre','desc')
			 					->limit(150)
			 					->get()

		 						);

			}
		
		 	
		}


		public function token(){

			 $array=Array( "token" => csrf_token() );

			 echo json_encode($array);
		}


		/*busca  los productos registrados, y se filtra por nombre.*/
		public function productos(Request $request){

			if($request->linea!='' && $request->marca!='' && $request->modelo!='' && $request->subcategoria!='' && $request->motor!=''){
		
			echo json_encode(

					/*Con almacenes
					Productos::select(\DB::raw('producto.* , producto.año as anio , almacen_producto.*'))
								->join('almacen_producto','almacen_producto.id_producto','=','producto.id')
			 					->join('almacen','almacen_producto.id_almacen','=','almacen.id_almacen')
			 					->where('almacen.status','=',1)
			 					->where('almacen.nombre','like',"%%".$request->almacen."%%")
			 					->where('producto.linea','like',"%%".$request->linea."%%")
			 					->where('producto.marca','like',"%%".$request->marca."%%")			 					
			 					->where('producto.modelo','like',"%%".$request->modelo."%%")			 					
			 					->where('producto.año','like',"%%".$request->anio."%%")
								->where('producto.id_subcategoria','=',$request->subcategoria)
								->where('producto.motor','like',"%%".$request->motor."%%")
			 					->orderBY('producto.nombre','desc')
			 					->limit(150)
			 					->get()

							);*/
							
					Productos::select(\DB::raw('producto.* , producto.año as anio'))
			 					->where('producto.linea','like',"%%".$request->linea."%%")
			 					->where('producto.marca','like',"%%".$request->marca."%%")			 					
			 					->where('producto.modelo','like',"%%".$request->modelo."%%")			 					
			 					->where('producto.año','like',"%%".$request->anio."%%")
								->where('producto.id_subcategoria','=',$request->subcategoria)
								->where('producto.motor','like',"%%".$request->motor."%%")
			 					->orderBY('producto.nombre','desc')
			 					->limit(150)
			 					->get()

							);

			}
			else{
				if($request->promocion!=''){
					echo json_encode(
						
						/*Con almacenes
						Productos::select(\DB::raw('producto.* , producto.año as anio  , almacen_producto.*'))
									->join('almacen_producto','almacen_producto.id_producto','=','producto.id')
									->join('almacen','almacen_producto.id_almacen','=','almacen.id_almacen')
									->where('almacen.status','=',1)
									->orderBY('producto.nombre','desc')
									->limit(150)
									->get()
											);
						*/
						
							Productos::select(\DB::raw('producto.* , producto.año as anio '))
										->where('producto_promocion','1')
										->orderBY('producto.nombre','desc')
										->limit(150)
										->get()
										);
						
				}
				else{
				
					echo json_encode(
							
								Productos::select(\DB::raw('producto.* , producto.año as anio '))
											->orderBY('producto.nombre','desc')
											->limit(150)
											->get()
											);
					}
			  }		
		
		 	
		}



		public function linea(Request $request){

             if($request->marca!=''){
				
				if($request->subcategoria != ''){
						echo json_encode(
						Productos::select(\DB::raw('distinct linea as label, linea as id'))
									->where('marca','=',$request->marca)
									->where('id_subcategoria','=',$request->subcategoria)
									->get()
	
						);
				}
				else{
						echo json_encode(
						Productos::select(\DB::raw('distinct linea as label, linea as id'))
									->where('marca','=',$request->marca)
									->get()
	
						);
				}		
						
		       }else{

					echo json_encode(
					Productos::select(\DB::raw('distinct linea as label, linea as id'))
								->get()

					);

		       }
		}


		public function marca(Request $request){

			if($request->linea!=''){

			if($request->subcategoria != ''){
						echo json_encode(
						Productos::select(\DB::raw('distinct linea as label, linea as id'))
									->where('linea','=',$request->linea)
									->where('id_subcategoria','=',$request->subcategoria)
									->get()
	
						);
				}
				else{
						echo json_encode(
						Productos::select(\DB::raw('distinct linea as label, linea as id'))
									->where('linea','=',$request->linea)
									->get()
	
						);
				}		
		   }else{

		   	echo json_encode(

				Productos::select(\DB::raw('distinct marca as label , marca as id'))
							->get()

				);
		   }
		}


		public function modelo(Request $request){
			if($request->subcategoria != ''){
					echo json_encode(
	
					Productos::select(\DB::raw(" distinct concat (modelo, ' / ', año) as label, concat (modelo, ' / ', año) as id"))
								->where('linea','=',$request->linea)
								->where('marca','=',$request->marca)
								->where('id_subcategoria','=',$request->subcategoria)
								->get()
								
					);
			}
			else{
					echo json_encode(
	
					Productos::select(\DB::raw(" distinct concat (modelo, ' / ', año) as label, concat (modelo, ' / ', año) as id"))
								->where('linea','=',$request->linea)
								->where('marca','=',$request->marca)
								->get()
								
					);
			}		
		}




//-------Registrar compra contra entrega------------------------------------
		public function saveCompra(Request $data){

				$compra = new Compras;
				//$compra->id_almacen = 1;
				$compra->id_usuario = $data->id_usuario;
				$compra->direccion_envio = $data->address;
				$compra->id_producto = $data->id_producto;
			    $compra->solicitado = $data->solicitado;
			    $compra->precio = $data->precio;
			    $compra->fecha_solicitud = $data->fecha;
				$msj = "Ocurrió un error al registrar la compra!";
				$nroCompra = NULL;
				$producto = Productos::where('id','=',$data->id_producto)->first(); 
				$guardado = 0;
			    if($compra->save()){
				 $idCompra = $compra->where('id_usuario','=',$data->id_usuario)->max('id');
				 $msj = "Compra registrada con éxito!";
				 $nroCompra = $idCompra;
				 $guardado = 1;
				 $asunto = "Confirmación de compra en ParteYá";
				 $contenido = "Usted realizó una compra a través de la aplicación movil de PartesYá
				 			   \nProducto: ".$producto->nombre."
							   \nPrecio producto: ".$data->precio."
							   \nCantidad: ".$data->solicitado."
							   \nTotal: ".($data->precio*$data->solicitado)."
							   \nDirección de envío: ".$data->address;
				 $to = $data->email;		   
				 $this->enviarEmailaUsuaio($asunto,$contenido,$to);
				}
			    echo json_encode(["guardado"=>$guardado,"msj" =>$msj,"nroCompra"=>$nroCompra]);

		}
//-------Consultar compra------------------------------------	
   public function getCompra(Request $data){
   		if($data->idCompra != ''){
			$compra = Compras::select(\DB::raw('compras.* , producto.nombre as productName'))
					 ->join('producto','compras.id_producto','=','producto.id')
					 ->where('compras.id','=',$data->idCompra)
					 ->get();  
			echo json_encode(["status"=>"ok","compra"=>$compra]);		 
		}
		else if($data->id_usuario != ''){
			$compra = Compras::select(\DB::raw('compras.id as idC,compras.* , producto.id as idP, producto.*'))
					 ->join('producto','compras.id_producto','=','producto.id')
					 ->where('compras.id_usuario','=',$data->id_usuario)
					 ->orderBY('compras.fecha_solicitud','desc')
					 ->get();  
			echo json_encode($compra);		 
		}
		
   }

   public function motores(Request $data){

   	echo json_encode(

				Productos::select(\DB::raw('distinct motor as label , motor as id'))
							->get()
				);

   }
   
  public function departamentos(Request $data){

   	echo json_encode(
				\DB::table('departamentos')->select('nombre as name' , 'id as code')
							->get()
				);

   }
  
  public function ciudades(Request $data){

   	echo json_encode(

				\DB::table('ciudades')->select('nombre as name' , 'id as code')
							->where('departamento_id', '=', $data->idDpto)
							->get()
				);

   }


   public function categorias(Request $data){

    if($data->id!=''){

   	echo json_encode(

				Categorias::select(\DB::raw('distinct id, nombre, descripcion, url_imagen, status'))
				->where("id","=",$data->id)						
							->get()

				);
   }else{
	echo json_encode(

				Categorias::select(\DB::raw('distinct id, nombre, descripcion, url_imagen, status'))						
							->get()

				);

   }

   }

   public function subcategorias(Request $data){

      if($data->id_categoria!=''){

			echo json_encode(

					Categorias::select(\DB::raw('categorias.* , subcategorias.*'))
								->join('subcategorias','categorias.id','=','subcategorias.id_categoria')
			 					->where('subcategorias.status','=',1)
			 					->where('categorias.status','=',1)
			 					->where('subcategorias.id_categoria','=',$data->id_categoria)
			 					->get()
										);
			
			}else{
				echo json_encode(

					Categorias::select(\DB::raw('categorias.* , subcategorias.*'))
								->join('subcategorias','categorias.id','=','subcategorias.id_categoria')
			 					->where('subcategorias.status','=',1)
			 					->where('categorias.status','=',1)
			 					->get()
										);

			}

   }


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}



	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $data)
	{

		$user = new User;
		$user->name = $data->name;
		$user->email = trim($data->email);
		$user->password =  bcrypt( $data->password);
	    $user->status = 1;      
	    $user->rol =2;  
		//\DB::connection('bdwordpress')->table('users')->insert(['usuario'=>$data->name,'contrasenya'=>$user->password,'email'=>$user->email]);
		
		if( strlen( trim($data->name) ) == 0 )
		{
           $array=Array( "result"=> "Debe ingresar nombre valido","token" => "" , "codError" =>'404');
		   echo json_encode($array);
		   return;
		}
		

		if( strlen( trim($data->email) ) == 0 )
		{
           $array=Array( "result"=> "Debe ingresar un email valido","token" => "" , "codError" =>'404');
		   echo json_encode($array);
		   return;
		}
		//return response()->json(['datos' =>  'Despues de armado']);
 						$count = User::where('email', '=', $user->email)->whereNotNull('password')->count();
	 					if( $count*1 <= 0 ) 
	 					{

                                   //si hay ya un email creado que cuyo password es null

									$count = User::where('email', '=', $user->email)->count();                                  
									if( $count*1 > 0 ) 
	 								{	
									
	 									$userN = User::where('email', '=', $user->email)->first();
	 									$userN->password =  bcrypt( $data->password);
										$userN->status = 1;      
										$userN->rol =2; 
										$userN->save();
										$userN->remember_token =    $this->createToken($userN);  
										$userN->save();
										if(Auth::attempt(['email'=>$data->email,'password'=>$data->password,'status'=>1],true))
																		    {
			    		
																              	  $array=Array( "result"=> "Usuario creado con exito y autenticado","token" => $userN->remember_token, "name" => $userN->name,"id_usuario" => $userN->id,"email" => $userN->email, "codError" =>'200');
																			      echo json_encode($array);
											   								}else{
											   									  $array=Array( "result"=> "Error al autenticar el usuario","token" => "", "codError" =>'201');
																	              echo json_encode($array);
											   								}

	 								}else{

							             if ($user->save())
							              {
											$user->remember_token =    $this->createToken($user);  
											$user->save();

					   							if(Auth::attempt(['email'=>$data->email,'password'=>$data->password,'status'=>1],true))
												    {
										              	   $array=Array( "result"=> "Usuario creado con exito y autenticado","token" => $user->remember_token, "name" => $user->name,"id_usuario" => $user->id,"email" => $user->email, "codError" =>'200');
													      echo json_encode($array);
					   								}else{
					   									  $array=Array( "result"=> "Error al autenticar el usuario","token" => "", "codError" =>'201');
											              echo json_encode($array);
					   								}

										   }			    
										 else
										  {

										  	  $array=Array( "result"=> "Error al registrar el usuario","token" => "","codError" =>'406');
										      echo json_encode($array);
										   }
									}

						  }else{
 
							  	  $array=Array( "result"=> "El usuario ya se encuentra registrado","token" => "" ,"codError" =>'302');
								  echo json_encode($array);
						  }
}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function login(Request $datos)
	{

         if( strlen( trim($datos->email) ) == 0 )
		{
           $array=Array( "result"=> "Debe ingresar un email valido","token" => "" , "codError" =>'404');
		   echo json_encode($array);
		   return;
		}

		 if( strlen( trim($datos->password) ) == 0 )
		{
           $array=Array( "result"=> "Debe ingresar un password valido","token" => "" , "codError" =>'404');
		   echo json_encode($array);
		   return;
		}

		if(Auth::attempt(['email'=>$datos->email,'password'=>$datos->password,'status'=>1]) ){
		                    
                    $user = User::where('email', '=', $datos->email)->first();  
                    $user->remember_token =    $this->createToken($user); 
                    $user->save();
			        $array=Array( "result"=> "Autenticando","token" => $user->remember_token, "name" => $user->name,"id_usuario" => $user->id,"email" => $user->email,"codError" =>'200');
					echo json_encode($array);

			    } else{

			        $array=Array( "result"=> "Error al autenticar el usuario","token" => "","name" => $datos->email, "codError" =>'201');
					echo json_encode($array);

			        }
		
	}
	 /**
     * funcion para actualizar los datos de un usuario
     */
	  public function updateUser(Request $datos)
	  {

         $user = new User;
		 $user->email = trim($datos->email);
		/*$array=Array( "result"=> "Recibiendo datos ".$datos->email,"token" => "","codError" =>'201');
			      echo json_encode($array);
			      return;*/
		 $count = User::where('email', '=', $user->email)->count();
		 if( $count*1>= 1 )
		 {
              $user = User::where('email', '=', $datos->email)->first();
               
              $user->name = $datos->name;
              $user->address = $datos->address;
              $user->movil = $datos->movil;
			  $user->CEDULA = $datos->cedula;
			  $user->FECHA_NAC = $datos->fecha_nac;
			  $user->CIUDAD_ID = $datos->ciudad_id;
	              if($user->save()){
		              $array=Array( "result"=> "Datos modificados",
		               "token" => csrf_token(),
					   "cedula" => $datos->cedula,
		               "name" => $datos->name,
		               "address" => $datos->address,
		               "movil" => $datos->movil,
					   "fecha_nac" => $datos->fecha_nac,
		               "status" => $datos->status,
					   "ciudad_id" => $datos->ciudad_id,
		               "rol" => $datos->rol,
		               "codError" =>'200');
					  echo json_encode($array);
				  }else{
	                  $array=Array( "result"=> "Error al actualizar el perfil".$datos->email,"token" => "","codError" =>'201');
				      echo json_encode($array);

				  }
		 }
		 else
		 {
             
	              $array=Array( "result"=> "Error al consultar el usuario".$datos->email,"token" => "","codError" =>'201');
			      echo json_encode($array);

		 }
		 
	  }

     /**
     * funcion para obtener los datos de un usuario
     */
	  public function getUser(Request $datos)
	  {

         $user = new User;
		 $user->email = trim($datos->email);
		 $count = User::where('email', '=', $user->email)->count();
		 if( $count*1>= 1 )
		 {
              $user = User::where('email', '=', $datos->email)->first();
			  $dpto = NULL;	
			  $ciudad = NULL;	
			  if($user->CIUDAD_ID > 0 && $user->CIUDAD_ID != NULL){
			  	
				$ciudad = \DB::table('ciudades')->select(DB::raw('id as code, nombre as name'))
							->where('id', '=', $user->CIUDAD_ID)
							->first();
							
				$dpto= \DB::select('select departamentos.id as code, departamentos.nombre as name 
									from departamentos, ciudades
									where departamentos.id=ciudades.departamento_id
									and ciudades.id=:idCiudad',['idCiudad'=>$user->CIUDAD_ID]);

			  }
			  
              $array=Array( "result"=> "Datos encontrados",
               "token" => csrf_token(),
			   "cedula" => $user->CEDULA,
               "name" => $user->name,
               "address" => $user->address,
               "movil" => $user->movil,
			   "fecha_nac" => $user->FECHA_NAC,
               "status" => $user->status,
			   "ciudad_id" => $user->CIUDAD_ID,
			   "ciudad" => $ciudad,
			   "dpto" => $dpto,
               "rol" => $user->rol,
               "codError" =>'200');
			  echo json_encode($array);
		 }
		 else
		 {           
	              $array=Array( "result"=> "Error al consultar el usuario","token" => "","codError" =>'201');
			      echo json_encode($array);

		 }
		 
	  }

    /**
     * Almacen ranking.
     */
     public function setranking(Request $datos){

        // $user = new User;
		 $user = User::where('email', '=', $datos->email)->first();
		 
		if(isset($user) && $user->id > 0){
		   $cant = Taller_ranking::where('id_usuario', '=', $user->id)->where('id_taller', '=', $datos->id_almacen)->count();
		  //  echo json_encode($cant);
		   // return;
           if( $cant*1 >0 ){
                  $almacen_ranking = Taller_ranking::where('id_usuario', '=', $user->id)->where('id_taller', '=', $datos->id_almacen)->first();
                  $almacen_ranking->valoracion = $datos->valoracion;
                  $almacen_ranking->observacion = $datos->comentario;
                  $almacen_ranking->save();
                  $array=Array( "result"=> "Actualizacion de valoracion registrada", "codError" =>'200');
				  echo json_encode($array);
           }else{
				$almacen_ranking = new Taller_ranking;
				$almacen_ranking->id_usuario = $user->id;
				$almacen_ranking->id_taller = $datos->id_almacen;
                $almacen_ranking->valoracion = $datos->valoracion;
                $almacen_ranking->observacion = $datos->comentario;
                $almacen_ranking->save();
                $array=Array( "result"=> "Nueva valoracion registrada ", "codError" =>'200');
				echo json_encode($array);
           }

		}else{
                $array=Array( "result"=> "Ha ocurrido un error al valorar", "codError" =>'201');
				echo json_encode($array);

		}

	}

	 /**
     * Almacen ranking.
     */
      public function getValoracion(Request $request)
      {
           //cambiar campo por id de almacen
			$sumValoraciones = 	Taller_ranking::select(\DB::raw('sum(valoracion) as sum '))
							->where('id_taller','=',$request->id_almacen)
							->pluck('sum');
			$countValoracion = Taller_ranking::select(\DB::raw('count(*) as cont'))
							->where('id_taller','=',$request->id_almacen)
							->pluck('cont');
							//$resultValore = Array($sumValoraciones);
		   
		   if($countValoracion == 0)
		   {
                $array=Array( "result"=> $countValoracion, "codError" =>'200');
				echo json_encode($array);
		   }else{
                $ranking = $sumValoraciones/$countValoracion;
                $array=Array( "result"=> $ranking, "codError" =>'200');
				echo json_encode($array);
		   }

      }

  /**
     * Comentarios ranking.
     */
      public function getResets(Request $request)
      {

      	echo json_encode(
      	$resets = 	Password_reset_apk::select(\DB::raw('*'))
							->get()
							);
		
      }
      /**
     * Comentarios ranking.
     */
      public function getComentarios(Request $request)
      {

      	echo json_encode(
      	$comentarios = 	Taller_ranking::select(\DB::raw('users.name as usuario,taller_ranking.valoracion,taller_ranking.observacion,taller_ranking.id_usuario,taller_ranking.id_taller'))
							->join('users','users.id','=','taller_ranking.id_usuario')
							->where('taller_ranking.id_taller','=',$request->id_almacen)
							->get()
							);
		//echo json_encode($comentarios);
      }

      /**
     * sendig code
     */
      public function getCode(Request $request)
      {

        $user = User::where('email', '=', $request->email)->first();
		$codigo = substr( md5(rand()), 0, 6);

		if(!isset($user) || $user->id <= 0){
			return response()->json(['result' => "Error usuario no encontrado","codError" =>'201'],201); 
		}
       
        $cant = Password_reset_apk::where('email', '=', $request->email)->count();
        if( $cant*1 >= 1)
        {
          $resetp = Password_reset_apk::where('email', '=', $request->email)->first();
          $resetp->codigo = $codigo;
          $resetp->estado = 1;
          if(!$resetp->save())
           {
	        return response()->json(['result' => "Error al registrar el codigo de restablecimiento","codError" =>'302'],302); 
           }
        }else
        {
            $resetp = new Password_reset_apk;
            $resetp->email = $request->email;
            $resetp->codigo = $codigo;
            $resetp->estado = 1;
	           if(!$resetp->save())
	           {
		        return response()->json(['result' => "Error al registrar el codigo de restablecimiento","codError" =>'302'],302); 
	           }
        }

        $pathToFile="";
        $containfile=false;
        
        $asunto="PartesYÁ Código para restablecer contraseña";
        $contenido="Su Código para restablecer la contraseña en PartesYÁ : ".$codigo;
  

        $to = $request->email;
       // $to =  $destinatario;
        $from ='admin@partesya.compuzoft.com';
        $r = false;

        $r= Mail::raw( $contenido, function ($message) use ($asunto,$to, $containfile,$pathToFile) {
       	    $message->from('admin@partesya.compuzoft.com','PartesYA');
            $message->to($to)->subject($asunto);
            });
            
        if($r){  
               return response()->json(['result' => "Codigo enviado con exito","codError" =>'200'],200); 
           }
        else
          {            
             return response()->json(['result' => "Error al enviar el codigo de restablecimiento","codError" =>'201'],201);   
          }  

      }
	 //
	 public function enviarEmailaUsuaio($asunto,$contenido,$to){
	 	$pathToFile="";
        $containfile=false;

        $from ='admin@partesya.compuzoft.com';
        $r = false;

        $r= Mail::raw( $contenido, function ($message) use ($asunto,$to, $containfile,$pathToFile) {
       	    $message->from('admin@partesya.compuzoft.com','PartesYA');
            $message->to($to)->subject($asunto);
            });
		return $r;            
	 } 
     /**
     * Validar codigo.
     */
     public function validarCodigo(Request $request)
      {

         $user = User::where('email', '=', $request->email)->first();

		 if(!isset($user) || $user->id <= 0){
			return response()->json(['result' => "Error usuario no encontrado","codError" =>'201'],201); 
		 }
        
        $cant = Password_reset_apk::where('email', '=', $request->email)
									->where('codigo', '=', $request->codigo)
									->where('estado', '=', '1')
									->count();
        if( $cant*1 >= 1)
        {
          
          $resetp = Password_reset_apk::where('email', '=', $request->email)->first();
          if($resetp->codigo != $request->codigo || $resetp->estado!=1 )
          {
			  if($resetp->codigo != $request->codigo)
			   return response()->json(['result' => "Codigo no coincide"],302); 
			  else
			   return response()->json(['result' => "Codigo inactivo"],302); 
          }else{
             if($request->password != $request->password2)
             {
              return response()->json(['result' => "Las contraseñas no coinciden","codError" =>'302'],302); 
             }

				$user->password = bcrypt($request->password);
				if(!$user->save())  return response()->json(['result' => "Error al registrar nueva contraseña","codError" =>'302'],302); 
				$resetp->estado = 0;
				if(!$resetp->save()) return response()->json(['result' => "Error al inactivar codigo ","codError" =>'302'],302); 

				return response()->json(['result' => "Datos restablecidos con exito","codError" =>'200'],200);
          	}

         }else{
         	return response()->json(['result' => "Codigo no activo ","codError" =>'302'],302); 
         }


      }

     public function userMensaje(Request $request)
     {
       
  		if( strlen( trim($request->email) ) == 0 )
		{
           $array=Array( "result"=> "Datos invalidos", "codError" =>'401');
		   echo json_encode($array);
		   return;
		}

		if( strlen( trim($request->nombre) ) == 0 )
		{
           $array=Array( "result"=> "Datos invalidos", "codError" =>'402');
		   echo json_encode($array);
		   return;
		}

		if( strlen( trim($request->asunto) ) == 0 )
		{
           $array=Array( "result"=> "Datos invalidos", "codError" =>'403');
		   echo json_encode($array);
		   return;
		}

		if( strlen( trim($request->comentario) ) == 0 )
		{
           $array=Array( "result"=> "Datos invalidos", "codError" =>'404');
		   echo json_encode($array);
		   return;
		}

        $user = User::where('email', '=', $request->email)->first();

		 if(!isset($user) || $user->id <= 0)
		 {
			return response()->json(['result' => "Error usuario no encontrado","codError" =>'201'],201); 
		 }

		$pathToFile="";
        $containfile=false;
        
        $asunto=$request->asunto;
        $contenido=$request->comentario;
  

       // $to = $request->email;
        $to =  'servicioalcliente@partesya.com.co';
        $from = $request->email;
        $nombre = $request->nombre;
        $r = false;

        $r= Mail::raw( $contenido, function ($message) use ($asunto,$to,$from,$containfile,$pathToFile,$nombre) {
       	    $message->from($from,$nombre);
            $message->to($to)->subject($asunto);
            });
            
        if($r){  
               //return response()->json(['result' => "Mensaje enviado con exito","codError" =>'200'],200); 
               $array=Array( "result"=> "Mensaje enviado con exito", "codError" =>'200');
		       echo json_encode($array);
           }
        else
          {            
              $array=Array( "result"=> "Error al enviar el mensaje", "codError" =>'201');
		   echo json_encode($array);
            // return response()->json(['result' => "Error al enviar el mensaje de restablecimiento","codError" =>'201'],201);   
          }  

     } 

	 /**
     * Login with provider
     */
public function redirectToProvider(Request $request)
    {
        return Socialite::driver('google')->redirect();
    }
	 /**
     * Login with provider
     */
public function handleProviderCallback(Request $request)
    {
        $data = Socialite::driver($request->provider)->user();
        // $user->token;
    }

    /**
     * Generate JSON Web Token.
     */
    protected function createToken($user)
    {
        $payload = [
            'sub' => $user->id,
            'iat' => time(),
            'exp' => time() + (2 * 7 * 24 * 60 * 60)
        ];
        return JWT::encode($payload,Config::get('app.token_secret'));
    }

	 /**
     * Login with Google.
     */
    public function google(Request $request)
    {
        $client = new GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ),));
        $params = [
            'code' => $request->input('code'),
            'client_id' => $request->input('clientId'),
            'client_secret' => Config::get('app.google_secret'),
            'redirect_uri' => $request->input('redirectUri'),
            'grant_type' => 'authorization_code',
        ];
        // Step 1. Exchange authorization code for access token.
        $accessTokenResponse = $client->request('POST', 'https://accounts.google.com/o/oauth2/token', [
            'form_params' => $params
        ]);
        $accessToken = json_decode($accessTokenResponse->getBody(), true);
        // Step 2. Retrieve profile information about the current user.
        $profileResponse = $client->request('GET', 'https://www.googleapis.com/plus/v1/people/me/openIdConnect', [
            'headers' => array('Authorization' => 'Bearer ' . $accessToken['access_token'])
        ]);
        $profile = json_decode($profileResponse->getBody(), true);
       // return response()->json(['datos' =>  $profile]);
        // Step 3a. If user is already signed in then link accounts.
        if ($request->header('Authorization'))
        {
            //consultar si el perfil existe
            $user = User::where('google', '=', $profile['sub'])->first();
            $token = explode(' ', $request->header('Authorization'))[1];
            if ( isset($user) )
            {
                return response()->json(['token' => $token,'id_usuario' => $user->id,'name' => $profile['name'],'email' => $profile['email'],'picture' => $profile['picture'] ,'codError' => '200']);
               // return response()->json(['message' => 'There is already a Google account that belongs to you'], 409);
            }else{
               //consultar si el email existe  
               $userG = User::where('email', '=', $profile['email'])->first();
               if(isset($userG) && $userG->id>0){

                $userG->google = $profile['sub'];
	            $userG->name = $userG->name ?: $profile['name'];
	            $userG->picture = $userG->picture ?: $profile['picture'];
	            $userG->remember_token =$this->createToken($userG);
	            $userG->save();
                return response()->json(['token' =>  $userG->remember_token,'id_usuario' => $userG->id,'name' => $profile['name'],'email' => $profile['email'],'picture' => $profile['picture'] ,'codError' => '200']);
               }else{
						$user = new User;
			            $user->google = $profile['sub'];
			            $user->name = $profile['name'];
			            $user->picture = $profile['picture'];
			            $user->email = $profile['email'];
			            $user->status = 1;      
	    				$user->rol =2;  
	    				$user->remember_token =$this->createToken($user);
			            $user->save();
			            return response()->json(['token' => $user->remember_token,'id_usuario' =>$user->id,'name' => $profile['name'],'email' => $profile['email'],'picture' => $profile['picture'] ,'codError' => '200']);
               
               }

         

            }
            
        }
        // Step 3b. Create a new user account or return an existing one.
        else
        {
            $user = User::where('google', '=', $profile['sub'])->first();
            if (isset($user))
            {
                return response()->json(['token' => $user->remember_token,'id_usuario' => $user->id,'name' => $profile['name'],'email' => $profile['email'],'picture' => $profile['picture'] ,'codError' => '200']);
            }else{

	            //consultar si el email existe  
               $userG = User::where('email', '=', $profile['email'])->first();
               if(isset($userG) && $userG->id>0){

	                $userG->google = $profile['sub'];
		            $userG->name = $userG->name ?: $profile['name'];
		            $userG->picture = $userG->picture ?: $profile['picture'];
		            $userG->remember_token =$this->createToken($userG);
		            $userG->save();
	                return response()->json(['token' => $userG->remember_token,'id_usuario' => $userG->id,'name' => $profile['name'],'email' => $profile['email'],'picture' => $profile['picture'] ,'codError' => '200']);
	             }else{
						$user = new User;
			            $user->google = $profile['sub'];
			            $user->name = $profile['name'];
			            $user->picture = $profile['picture'];
			            $user->email = $profile['email'];
			            $user->status = 1;      
	    				$user->rol =2;  
	    				$user->remember_token =$this->createToken($user);
			            $user->save();
			            return response()->json(['token' => $user->remember_token,'id_usuario' => $user->id,'name' => $profile['name'],'email' => $profile['email'],'picture' => $profile['picture'] ,'codError' => '200']);
               
               }
            }
        }
    }

     /**
     * Login with Facebook.
     */
    public function facebook(Request $request)
    {
        $client = new GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ),));
        $params = [
            'code' => $request->input('code'),
            'client_id' => $request->input('clientId'),
            'redirect_uri' => $request->input('redirectUri'),
            'client_secret' => Config::get('app.facebook_secret')
        ];

       //  return response()->json(['datos' => $params]);
        // Step 1. Exchange authorization code for access token.
        $accessTokenResponse = $client->request('GET', 'https://graph.facebook.com/v2.5/oauth/access_token', [
            'query' => $params
        ]);
        $accessToken = json_decode($accessTokenResponse->getBody(), true);

        // Step 2. Retrieve profile information about the current user.
       $fields = 'id,email,first_name,last_name,link,name,picture';
        $profileResponse = $client->request('GET', 'https://graph.facebook.com/v2.5/me', [
            'query' => [
                'access_token' => $accessToken['access_token'],
                'fields' => $fields
            ]
        ]);
        $profile = json_decode($profileResponse->getBody(), true);

        //$profile['name'] = $profile['first_name'].' '.$profile['last_name'];
         $profile['picture'] = $profile['picture']['data']['url'];
     
        // Step 3a. If user is already signed in then link accounts.
         if ($request->header('Authorization'))
        {
             
            $user = User::where('facebook', '=', $profile['id'])->first();
            $token = explode(' ', $request->header('Authorization'))[1];
            if ( isset($user) )
            {
                return response()->json(['token' => $token,'id_usuario'=> $user->id ,'name' => $profile['name'],'email' => $profile['email'],'picture' => $profile['picture'] ,'codError' => '200']);
               // return response()->json(['message' => 'There is already a Google account that belongs to you'], 409);
            }else{
               //consultar si el email existe  
               $userG = User::where('email', '=', $profile['email'])->first();
               if(isset($userG) && $userG->id>0){

                $userG->facebook = $profile['id'];
	            $userG->name = $userG->name ?: $profile['name'];
	            $userG->picture = $userG->picture ?: $profile['picture'];
	            $userG->remember_token =$this->createToken($userG);
	            $userG->save();
                return response()->json(['token' => $userG->remember_token,'id_usuario' => $userG->id ,'name' => $profile['name'],'email' => $profile['email'],'picture' => $profile['picture'] ,'codError' => '200']);
               }else{
						$user = new User;
			            $user->facebook = $profile['id'];
			            $user->name = $profile['name'];
			            $user->picture = $profile['picture'];
			            $user->email = $profile['email'];
			            $user->status = 1;      
	    				$user->rol =2;  
	    				$user->remember_token =$this->createToken($user);
			            $user->save();
			            return response()->json(['token' => $user->remember_token,'id_usuario'=> $user->id ,'name' => $profile['name'],'email' => $profile['email'],'picture' => $profile['picture'] ,'codError' => '200']);
               
               }

         

            }
           
        }
        // Step 3b. Create a new user account or return an existing one.
        else
        {
        	 //return response()->json(['datos' => 'Entra en 1']);
              $user = User::where('facebook', '=', $profile['id'])->first();
            if (isset($user))
            {
                return response()->json(['token' => $user->remember_token,'id_usuario'=>$user->id,'name' => $profile['name'],'email' => $profile['email'],'picture' => $profile['picture'] ,'codError' => '200']);
            }else{

	            //consultar si el email existe  
               $userG = User::where('email', '=', $profile['email'])->first();
               if(isset($userG) && $userG->id>0){

	                $userG->facebook = $profile['id'];
		            $userG->name = $userG->name ?: $profile['name'];
		            $userG->picture = $userG->picture ?: $profile['picture'];
		            $userG->remember_token =$this->createToken($userG);
		            $userG->save();
	                return response()->json(['token' => $userG->remember_token,'id_usuario'=> $userG->id ,'name' => $profile['name'],'email' => $profile['email'],'picture' => $profile['picture'] ,'codError' => '200']);
	             }else{
						$user = new User;
			            $user->facebook = $profile['id'];
			            $user->name = $profile['name'];
			            $user->picture = $profile['picture'];
			            $user->email = $profile['email'];
			            $user->status = 1;      
	    				$user->rol =2;  
	    				$user->remember_token =$this->createToken($user);
			            $user->save();
			            return response()->json(['token' => $user->remember_token,'id_usuario'=> $user->id ,'name' => $profile['name'],'email' => $profile['email'],'picture' => $profile['picture'] ,'codError' => '200']);
               
               }
            }
        }
    }//fin si es facebook

    /**
     * Login with Twitter.
     */
    public function twitter(Request $request)
    {
     //  return response()->json(['datos' => 'Entra en 1']);
//array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ),)
       $stack = GuzzleHttp\HandlerStack::create();

        // Part 1 of 2: Initial request from Satellizer.
        if (!$request->input('oauth_token') || !$request->input('oauth_verifier'))
        {
            $stack = GuzzleHttp\HandlerStack::create();
            $requestTokenOauth = new Oauth1([
              'consumer_key' => Config::get('app.twitter_key'),
              'consumer_secret' => Config::get('app.twitter_secret'),
              'callback' => $request->input('redirectUri'),
              'token' => '',
              'token_secret' => ''
            ]);
            $stack->push($requestTokenOauth);
            $client = new GuzzleHttp\Client([
                'handler' => $stack,
                'curl' => array( CURLOPT_SSL_VERIFYPEER => false, )
            ]);
            // Step 1. Obtain request token for the authorization popup.
            $requestTokenResponse = $client->request('POST', 'https://api.twitter.com/oauth/request_token', [
                'auth' => 'oauth'
            ]);
            $oauthToken = array();
            parse_str($requestTokenResponse->getBody(), $oauthToken);
            // Step 2. Send OAuth token back to open the authorization screen.
            return response()->json($oauthToken);
        }
        // Part 2 of 2: Second request after Authorize app is clicked.
        else
        {
            
            $accessTokenOauth = new Oauth1([
                'consumer_key' => Config::get('app.twitter_key'),
                'consumer_secret' => Config::get('app.twitter_secret'),
                'token' => $request->input('oauth_token'),
                'verifier' => $request->input('oauth_verifier'),
                'token_secret' => ''
            ]);
            $stack->push($accessTokenOauth);
            $client = new GuzzleHttp\Client([
                'handler' => $stack,
                 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, )
            ]);
            // Step 3. Exchange oauth token and oauth verifier for access token.
            $accessTokenResponse = $client->request('POST', 'https://api.twitter.com/oauth/access_token', [
                'auth' => 'oauth'
            ]);
            $accessToken = array();
            parse_str($accessTokenResponse->getBody(), $accessToken);
            $profileOauth = new Oauth1([
                'consumer_key' => Config::get('app.twitter_key'),
                'consumer_secret' => Config::get('app.twitter_secret'),
                'oauth_token' => $accessToken['oauth_token'],
                'token_secret' => ''
            ]);
            $stack->push($profileOauth);
            $client = new GuzzleHttp\Client([
                'handler' => $stack,
                 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, )
            ]);
            // Step 4. Retrieve profile information about the current user.
            $profileResponse = $client->request('GET', 'https://api.twitter.com/1.1/users/show.json?screen_name=' . $accessToken['screen_name'], [
                'auth' => 'oauth'
            ]);
            $profile = json_decode($profileResponse->getBody(), true);

            // Step 5a. Link user accounts.
             $email = '@'.$profile['screen_name'];
            if ($request->header('Authorization'))
            {
               
           // return response()->json(['datos' => 'Entra en 1']);
 			$user = User::where('twitter', '=', $profile['id'])->first();
 			 //return response()->json(['datos' => 'Entra en 1']);
            $token = explode(' ', $request->header('Authorization'))[1];

            if ( isset($user) )
            {
                return response()->json(['token' => $token,'id_usuario' => $user->id,'name' => $profile['screen_name'],'email' => $email ,'codError' => '200']);
               // return response()->json(['message' => 'There is already a Google account that belongs to you'], 409);
            }else{
               //consultar si el email existe  
              $userG = User::where('email', '=', $email)->first();
               if(isset($userG) && $userG->id>0){

                $userG->twitter = $profile['id'];
	            $userG->name = $userG->name ?: $profile['screen_name'];
	            $userG->email = $email; 
	            $userG->remember_token =$this->createToken($userG);
	            $userG->save();
                return response()->json(['token' =>  $userG->remember_token,'id_usuario' => $userG->id,'name' => $profile['name'],'email' => $email ,'codError' => '200']);
               }else{
						$user = new User;
			            $user->twitter = $profile['id'];
			            $user->name = $profile['screen_name'];
			            $user->email = $email;
			            $user->status = 1;      
	    				$user->rol =2;  
	    				$user->remember_token =$this->createToken($user);
			            $user->save();
			            return response()->json(['token' => $user->remember_token,'id_usuario' => $user->id ,'name' => $profile['screen_name'],'email' => $email ,'codError' => '200']);
               
               }
         
             }

            }
            // Step 5b. Create a new user account or return an existing one.
            else
            {
             //  return response()->json(['datos' => 'Entra en 2']);
               $user = User::where('twitter', '=', $email)->first();
	            if (isset($user))
	            {
	                return response()->json(['token' => $user->remember_token,'id_usuario' => $user->id,'name' => $profile['screen_name'],'email' => $email,'codError' => '200']);
	            }else{

		            //consultar si el email existe  
	             $userG = User::where('email', '=', $email)->first();
	               if(isset($userG) && $userG->id>0){

		                $userG->twitter = $profile['id'];
			            $userG->name = $userG->name ?: $profile['screen_name'];
			            $userG->email = $email; 
			            $userG->remember_token =$this->createToken($userG);
			            $userG->save();
		                return response()->json(['token' => $userG->remember_token,'id_usuario' => $userG->id,'name' => $profile['screen_name'],'email' => $email,'codError' => '200']);
		             }else{
							$user = new User;
				            $user->twitter = $profile['id'];
				            $user->name = $profile['screen_name'];
				            $user->email = $email;
				            $user->status = 1;      
		    				$user->rol =2; 
		    				$user->remember_token =$this->createToken($user); 
				            $user->save();
				            return response()->json(['token' => $user->remember_token,'id_usuario' => $user->id,'name' => $profile['screen_name'],'email' => $email,'codError' => '200']);
	               
	              }
             	}
           }
        }
    }//fin de si es twitter


}
