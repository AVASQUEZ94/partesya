<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use Session;
use Redirect;
use App\User;
use App\RecuperarPassword;
use Mail;
use App\HistoricoSesion;
class loginController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	
	
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $datos)
	{
		//



	  $view="Inicio";
      $title="Inicio";

		if(Auth::attempt(['email'=>$datos->username,'password'=>$datos->password,'status'=>1])){
						$histS= HistoricoSesion::Create([
						"id_usuario"	=> Auth::user()->id,
						"correo"	=> $datos->username,
						"nombre" =>  Auth::user()->name
						]);


			         return Redirect::to('/home')->with(compact("view","title"));

			    } else{

			         	Session::flash('message-error','Usuario o contraseña incorrecta');
				        return Redirect::to('/')->with(compact("view","title"));

			             }
		
		


	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

		public function logout(){


            Auth::logout();
            
            return Redirect::to('/');

             }






   public function recuperarclave(Request $request){

		return view("menu.recuperar.index");

	}



	public function correo(Request $request){


		if(trim($request->username)==""){
			Session::flash('alert','Ingrese el nombre de su usuario');
			return Redirect::back();
		}
		else
			if(User::where("email","=",$request->username)->count("id")==0){
					Session::flash('alert','El usuario no existe en nuestra base de datos');
					return Redirect::back();
			}

		$user=User::select(\DB::raw("md5(email) as pw"))->where("email","=",$request->username)->lists("pw");

		$correo=User::select("email")->where("email","=",$request->username)->lists("email");
			$_SESSION['mail']=$correo;

		
		$v=	Mail::send('menu.recuperar.contact',["pass"=>$user],function($msj){
			$msj->subject('PartesYa - Correo de Recuperación de Contraseña.');			
				$too=$_SESSION['mail'];
				$msj->to(''.$too[0].'');
		});

	if($v){
		
		$rec= new RecuperarPassword;
		$rec->token=$user[0];
		$id=User::select("id")->where("email","=",$request->username)->lists("id");
		$rec->id_usuario=$id[0];
		$rec->save();
		Session::flash('message','Felicitaciones - Correo enviado! siga el enlace enviado a su correo');

			return Redirect::back();
	}
	else
		Session::flash('alert','Error al enviar correo');

			return Redirect::back();



	}


		public function forget(Request $request){


		$recup=RecuperarPassword::select("id_usuario")->where("token","=",$request->us)->whereRaw("minute(timeDIFF(now(),(created_at - INTERVAL 6 HOUR)))<10")->max("id");

		if($recup){
					$re=RecuperarPassword::find($recup);
					$recup=$re->id_usuario;
			return view ("menu.recuperar.cambiarpw")->with(compact("recup"));
		}
		else{
			Session::flash('alert','El tiempo del enlace expiró.');
			return Redirect::to("/");
		}
	}





	public function guardarnewpw(Request $request){

			if(trim($request->password)==0)
			{

				  Session::flash('Alert','Ingrese la contraseña');
			
			                return Redirect::back();
			}
		
			$user=User::find($request->userid);
			$user->password=bcrypt($request->password);

			if($user->save()){


				  if(Auth::attempt(['email'=>$user->email,'password'=>$request->password, "status" =>1])){
			
			
			          Session::flash('message','Felicitaciones, su contraseña se ha restablecido exitosamente, Bienvenido al Menú.');
			
			                     return Redirect::to('/home');
			
			
			              }else{
									 Session::flash('alert','no se pudo iniciar sesion.');
			
			                return Redirect::back();
			
			

			                   } 


			}
			else { 
 Session::flash('alert','Error.');

				return Redirect::back();}
			


	}




}
