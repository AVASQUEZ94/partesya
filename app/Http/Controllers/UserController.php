<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\EditUserRequest;
use Illuminate\Http\Request;
use App\User;
use App\HistoricoUsuario;
use Auth;
use Session;
use Redirect;
class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 public function __construct()
  {
    $this->middleware('auth');
  }
  
	public function index(Request $request)
	{
		//



     $status = $request->status;

        if($request->status=="inactive" )
             $s_status=0;

         else 
             $s_status=1;


		 $users=User::where('status', '=', $s_status)
                  ->where('name', 'Like', '%%'.$request->s_name.'%%')
                  ->where('email', 'Like', '%%'.$request->s_email.'%%')
                  ->paginate(15);


     $users->setPath('user-system?s_name='.$request->s_name.'&s_email='.$request->s_email.'&status='.$request->status);



      $view="LISTA DE USUARIOS";
      $title="Usuarios";
	   	return view("menu.usersystem.listUsers")->with(compact('users','status','request',"title","view"));


	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//

       $view="CREAR USUARIO";
      $title="Usuarios";
		return view("menu.usersystem.createUser")->with(compact("view","title"));


	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateUserRequest $data)
	{
		//

		$user = new User;


		    $user->name = $data->name;
		    $user->address = $data->address;		   
		    $user->movil = $data->movil;
		    $user->email = $data->email;
        $user->password =  bcrypt( $data->password);
        $user->status = 1;

        if(isset($data->rol))

            $user->rol = $data->rol;

        else 

            $user->rol =2; 

      


    if ($user->save()){



$nuevo = new HistoricoUsuario;

        $nuevo->id_usuario = $user->id;
        $nuevo->nombre = $data->name;
        $nuevo->address = $data->address;       
        $nuevo->movil = $data->movil;
        $nuevo->email = $data->email;
        $nuevo->password =  bcrypt( $data->password);
        $nuevo->status = 1;
        $nuevo->tipo_transaccion = "Agregado";


        if(isset($data->rol))

            $nuevo->rol = $data->rol;

        else {$nuevo->rol =2;}
            


          $nuevo->save(); 

         if (Auth::check()){ //in case user has been register by panel control
            $view="LISTA DE USUARIOS";
            $title="Usuarios";
               Session::flash('message','Usuario creado con éxito!');
               if(isset($_SESSION['lastPage']))

               return Redirect::to('/user-system?page='.$_SESSION['lastPage'])->with(compact("view","title"));

           		else
           			  return Redirect::to('/user-system')->with(compact("view","title"));

         }else{  //in case user has been register by create new account, redirect and login automatic for the new member


              if(Auth::attempt(['email'=>$data->email,'password'=>$data->password,'status'=>1])){

                $view="Bienvenido al MENU";
                $title="Bienvenido";
                    Session::flash('message','Congratulations '.$data->name.', Your account has been created successfully, Welcome to your dashboard.');

                     return Redirect::to('/principal')->with(compact("view","title"));



              }else{


                return Redirect::back();


                   } 


            }


           


          
  
    }else{

          Session::flash('message-error','Usuario no pudo ser creado');
            $view="CREAR USUARIO";
            $title="Usuarios";
            if (Auth::check()) 

                return Redirect::to('/user-system/create')->with(compact("view","title"));


            else 

                return Redirect::back();


          }

          
       




	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	 public function edit($id){


            $user=User::find($id);

            $view="EDITAR USUARIO";
            $title="Usuarios";
      
            return view('menu.usersystem.editUser')->with(compact('user',"view","title"));

   				 }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	
  public function update(EditUserRequest $data, $id){



        $user=User::find($id);

        $user->name = $data->name;
        $user->address = $data->address;
        $user->rol = $data->rol;
        $user->movil = $data->movil;
        $user->email = $data->email;



         if($data->status)
          
              $user->status = 1;

         else

              $user->status = 0;

          
        if(!isset($data->password) && $data->password!="" )     
                  $user->password =  bcrypt($data->password);


        if ($user->save()){
           $view="LISTA DE USUARIOS";
            $title="Usuarios";

               Session::flash('message','Los datos del usuario fueron actualizados con éxito!');
               if(isset($_SESSION['pageCurrent']))

               return Redirect::to('/user-system?page='.$_SESSION['pageCurrent'])->with(compact("view","title"));
           		else
           	 return Redirect::to('/user-system')->with(compact("view","title"));

        }else{
           $view="EDITAR USUARIO";
            $title="Usuarios";

                Session::flash('message-error','Usuario no pudo ser actualizado');
  
                return Redirect::back()->with(compact("view","title"));
        }


    }


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	     public function destroy($id){

        $usuario= User::find($id);
          HistoricoUsuario::create([
            "id_usuario"=>$id,
            "nombre"  => $usuario->name,
            "address"  => $usuario->address,
            "movil"  => $usuario->movil,
            "email" => $usuario->email,
            "password" =>  $usuario->password,
            "status" => $usuario->status,
            "rol"  => $usuario->rol,
            "google"=>$usuario->google,
            "facebook"=>$usuario->facebook,
            "twiter" => $usuario->twiter,
            "displayname"  => $usuario->displayname,
            "miembro_hasta"=>$usuario->miembro_hasta,
            "tipo_transaccion"=>'Eliminado'
          ]);

        

     $destruir= User::destroy($id);
      $view="LISTA DE USUARIOS";
      $title="Usuarios";
        if($destruir){
          
                Session::flash('message','Usuaario eliminado con éxito!');
  
                return Redirect::to('/user-system')->with(compact("view","title"));

        }else{

                Session::flash('message-error','Usuario no pudo ser eliminado');
  
                return Redirect::to('/user-system')->with(compact("view","title"));

                   }


    } 				 												


}
