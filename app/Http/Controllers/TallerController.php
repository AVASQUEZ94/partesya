<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateRequestTaller;
use App\Http\Requests\UpdateRequestTaller;
use Illuminate\Http\Request;
use App\Taller;
use App\User;
use Redirect;
use Gmaps;
use App\TallerServicio;
use App\Servicio;
use App\TipoTaller;
use App\TallerImagen;
use App\HistoricoTaller;
use Auth;

use App\HistoricoTallerServicio;
class TallerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{


     $status = $request->status;

        if($request->status=="inactive" )
             $s_status=0;

         else 
             $s_status=1;


		 $users=Taller::where('status', '=', $s_status)
                  ->where('nombre', 'Like', '%%'.$request->s_name.'%%')
                  ->where('email', 'Like', '%%'.$request->s_email.'%%')
                  ->paginate(15);


     $users->setPath('talleresc?s_name='.$request->s_name.'&s_email='.$request->s_email.'&status='.$request->status);


    	$view="TODOS LOS TALLERES";
		$title="Taller";	
	   	return view("menu.taller.list")->with(compact('users','status','request',"view","title"));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		$view="CREAR TALLER";
		$title="Taller";
        $servis=Servicio::orderby("descripcion","asc")->get();
        $tipotaller=TipoTaller::orderby("descripcion","asc")->get();
		return view("menu.taller.create")->with(compact("servis","tipotaller","view","title"));


	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateRequestTaller $request)
	{

		$taller=Taller::Create(
						[
 						"nombre"	=> $request->nombre,
 						"nombre_dueno"	=> $request->nombre_dueno,
 						"nombre_gerente"	=> $request->nombre_gerente,
 						"direccion"	=> $request->direccion,
 						"tipo_taller"	=>  $request->tipo_taller,
 						"telefono_fijo"	=> $request->telefono_fijo,
 						"telefono_movil"	=> $request->telefono_movil,
 						"latitud"=>$request->latitud,
 						"longitud"=>$request->longitud,
 						"email"	=> $request->email,
 						"pagina_web"	=> $request->pagina_web
 						]);



			$ide=Taller::max("id");


		switch ($taller) {
			case false:
				return Redirect::back()->with("error","Taller no pudo ser guardado.");
				break;
			
			default:

			for ($i=0; $i <count($request->servicioofrecido) ; $i++) { 
					$taser=new TallerServicio;
					$taser->id_taller=$ide;
					$taser->id_servicio=$request->servicioofrecido[$i];
					$taser->save();
				
				}

			#verificar si cargaron imagenes en el taller
				

			if( $request->file('imagenes')[0]!=null){
				$cont=1;
				TallerImagen::destroy(TallerImagen::where("id_taller","=",$ide)->lists("id"));

					foreach ($request->file('imagenes') as $key ) {
					
						$fileName=($ide)."_".$cont.".".$key->getClientOriginalExtension();
						
				        $key->move("taller/gallery", $fileName);	            
					 		
						$cont++;

							$gallery=new TallerImagen;
							$gallery->file_name=$fileName;
							$gallery->id_taller=$ide;
							$gallery->save();


					}			
				}


				#registro en la base de datos historico

				$taller=HistoricoTaller::Create(
						[
 						"nombre"	=> $request->nombre,
 						"id_taller"=>$ide,
 						"nombre_dueno"	=> $request->nombre_dueno,
 						"nombre_gerente"	=> $request->nombre_gerente,
 						"direccion"	=> $request->direccion,
 						"tipo_taller"	=>  $request->tipo_taller,
 						"telefono_fijo"	=> $request->telefono_fijo,
 						"telefono_movil"	=> $request->telefono_movil,
 						"latitud"=>$request->latitud,
 						"longitud"=>$request->longitud,
 						"email"	=> $request->email,
 						"pagina_web"	=> $request->pagina_web,
 						"id_creador"=>Auth::user()->id,
 						"status" =>1,
 						"tipo_transaccion" =>"Agregado"
 						]);

				$ideT=HistoricoTaller::max("id");
					for ($i=0; $i <count($request->servicioofrecido) ; $i++) { 
					$taser=new HistoricoTallerServicio;
					$taser->id_taller=$ide;
					$taser->id_historico_taller=$ideT;
					$taser->id_servicio=$request->servicioofrecido[$i];
					$taser->save();
				
				}
				$view="TODOS LOS TALLERES";
				$title="Taller";
				return Redirect::to("talleresc")->with("message","Taller Guardado correctamente.",compact("view","title"));
				break;
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
			

		
	
        if(Taller::where("id","=",$id)->count("id")==0)
		{		$view="EDITAR TALLER";
				$title="Taller";
				return Redirect::back()->with('alert', 'Ha ocurrido un error, posiblemente el Taller que intentas buscar haya sido eliminado o no se encuentra disponible');			

		}else{
			$user=Taller::find($id);

			$servis=Servicio::orderby("descripcion","asc")->get();
           	
      		$tipotaller=TipoTaller::orderby("descripcion","asc")->get();
      		$view="EDITAR TALLER";
			$title="Taller";
            return view('menu.taller.edit')->with(compact('user',"servis","tipotaller","title","view"));
		}

               

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,UpdateRequestTaller $data)
	{
		

		
		$taller=Taller::findOrfail($id);

        $taller->nombre = $data->nombre;
        $taller->nombre_dueno = $data->nombre_dueno;
        $taller->nombre_gerente = $data->nombre_gerente;
        $taller->direccion = $data->direccion;
        $taller->tipo_taller = $data->tipo_taller;
        $taller->latitud=$data->latitud;
        $taller->longitud=$data->longitud;
        $taller->telefono_fijo = $data->telefono_fijo;
        $taller->telefono_movil = $data->telefono_movil;
        $taller->email = $data->email;
        $taller->pagina_web = $data->pagina_web;


         if($data->status)
          
              $taller->status = 1;

         else

              $taller->status = 0;


        switch ($taller->save()) {
        	case false:
				return Redirect::back()->with("error","Taller no pudo ser actualizado.");
				break;
			
			default:

			TallerServicio::destroy(TallerServicio::where("id_taller","=",$id)->lists("id"));
				
				for ($i=0; $i <count($data->servicioofrecido) ; $i++) { 
					$taser=new TallerServicio;
					$taser->id_taller=$id;
					$taser->id_servicio=$data->servicioofrecido[$i];
					$taser->save();
				
				}
			
					#verificar si cargaron imagenes en el taller

			if( $data->file('imagenes')[0]!=null){
				$cont=1;
				TallerImagen::destroy(TallerImagen::where("id_taller","=",$id)->lists("id"));

					foreach ($data->file('imagenes') as $key ) {
					
						$fileName=($id)."_".$cont.".".$key->getClientOriginalExtension();
						
				        $key->move("taller/gallery", $fileName);	            
					 		
						$cont++;

							$gallery=new TallerImagen;
							$gallery->file_name=$fileName;
							$gallery->id_taller=$id;
							$gallery->save();


					}			
				}
	
				return Redirect::back()->with("message","Taller actualizado correctamente.");
				break;
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
					$taller= Taller::find($id);
					HistoricoTaller::create([
						"nombre"	=> $taller->nombre,
 						"id_taller"=>$id,
 						"nombre_dueno"	=> $taller->nombre_dueno,
 						"nombre_gerente"	=> $taller->nombre_gerente,
 						"direccion"	=> $taller->direccion,
 						"tipo_taller"	=>  $taller->tipo_taller,
 						"telefono_fijo"	=> $taller->telefono_fijo,
 						"telefono_movil"	=> $taller->telefono_movil,
 						"latitud"=>$taller->latitud,
 						"longitud"=>$taller->longitud,
 						"email"	=> $taller->email,
 						"pagina_web"	=> $taller->pagina_web,
 						"id_creador"=>Auth::user()->id,
 						"status" =>$taller->status,
 						"tipo_transaccion"=>'Eliminado'
 					]);

				

		$doc=Taller::destroy($id);
			if($doc){
			return Redirect::to("/talleresc")->with('message', 'Registro Eliminado correctamente!');

		}else{
			return Redirect::back()->with('alert', 'Error al eliminar el resgitro.!');
		}




	}
						

	
}
