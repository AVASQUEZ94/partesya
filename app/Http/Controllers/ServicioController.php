<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\TipoTallerRequest;
use App\Http\Requests\UpdateTipoTallerRequest;
//use App\Taller;
use App\User;
use Redirect;
//use Gmaps;
//use App\TallerServicio;
use App\Servicio;
use App\TipoTaller;


class ServicioController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$registros=Servicio::orderby("descripcion","asc")->get();


	  $view="LISTA DE TIPOS DE SERVICIOS";
      $title="TIPO DE SERVICIO";

		return view("servicio.index")->with(compact("registros","view","title"));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	  $view="CREAR TIPOS DE SERVICIOS";
      $title="TIPO DE SERVICIO";
		return view("servicio.gestion")->with(compact("view","title"));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(TipoTallerRequest $request)
	{
		if(Servicio::whereRaw("UPPER(descripcion) = '".strtoupper($request->descripcion)."' ")->count("id")==0){
			$nuevo=new Servicio;
			$nuevo->descripcion=$request->descripcion;

			if($nuevo->save()){
				$view="LISTA DE TIPOS DE SERVICIOS";
      			$title="TIPO DE SERVICIO";
				return Redirect::to("/servicio")->with('message', 'Registro Agregado correctamente!',compact("view","title"));

			}else{
				 $view="CREAR TIPOS DE SERVICIOS";
      			$title="TIPO DE SERVICIO";
				return Redirect::back()->with('alert', 'Error al agregar el resgitro.!',compact("view","title"));
			}
		}else{

			 $view="CREAR TIPOS DE SERVICIOS";
      			$title="TIPO DE SERVICIO";
				return Redirect::back()->with('alert', 'Error ya se encuentra un registro con este nombre! '.$request->descripcion.'',compact("view","title"));
			}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$doc=Servicio::find($id);
		 $view="EDITAR TIPOS DE SERVICIOS";
      			$title="TIPO DE SERVICIO";
		return view("servicio.modificar")->with(compact("doc","title","view"));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(UpdateTipoTallerRequest $request,$id)
	{
		if(Servicio::whereRaw("UPPER(descripcion) = '".strtoupper($request->descripcion)."' ")->where("id","!=", $id)->count("id")==0){
			$doc=Servicio::find($id);
			$doc->descripcion=$request->descripcion;			
						
			if($doc->save()){
				$view="LISTA TIPOS DE SERVICIOS";
      			$title="TIPO DE SERVICIO";
				return Redirect::to("/servicio")->with('message', 'Registro Modificado correctamente!',compact("view","title"));

			}else{
				$view="EDITAR TIPOS DE SERVICIOS";
      			$title="TIPO DE SERVICIO";
				return Redirect::back()->with('alert', 'Error al modificar el resgitro.!',compact("view","title"));
			}
		}else{
			$view="EDITAR TIPOS DE SERVICIOS";
      			$title="TIPO DE SERVICIO";
				return Redirect::back()->with('alert', 'Error ya se encuentra un registro con este nombre! '.$request->descripcion.'',compact("view","title"));}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$view="EDITAR TIPOS DE SERVICIOS";
      	$title="TIPO DE SERVICIO";
		if(Servicio::count("id")==1){
			return Redirect::back()->with('alert', 'Error, Debe dejar almenos un registro en la base de datos.',compact("view","title"));
				}

		$doc=Servicio::destroy($id);
			if($doc){
				$view="LISTA TIPOS DE SERVICIOS";
      			$title="TIPO DE SERVICIO";
			return Redirect::to("/servicio")->with('message', 'Registro Eliminado correctamente!',compact("view","title"));

		}else{
			return Redirect::back()->with('alert', 'Error al eliminar el resgitro.!',compact("view","title"));
		}
	}

}
