<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {


 	if (Auth::check())

      	 return Redirect::to('/home');

    else
        return view('layout_sign.layout_login');
   

});

#login
Route::get('logout','loginController@logout');
Route::post('login','loginController@store');

Route::get('home','controllerIndex@index');

#total en el index para adminisstrador
Route::get('totalusuarios','HomeController@totalusuarios');
Route::get('totalproductos','HomeController@totalproductos');
Route::get('totaltaller','HomeController@totaltaller');
Route::get('totalalmacen','HomeController@totalalmacen');

Route::get('token','ApiController@token');

Route::post('apilogin','ApiController@login');
Route::post('AddUser','ApiController@store');

Route::group(['middleware' => 'cors'], function(){
	Route::post('google', 'ApiController@google');
	Route::post('facebook', 'ApiController@facebook');
	Route::post('twitter', 'ApiController@twitter');
});

Route::post('setranking', 'ApiController@setranking');
Route::get('getvaloracion', 'ApiController@getValoracion');
Route::get('getcomentarios', 'ApiController@getComentarios');

Route::get('getuser', 'ApiController@getUser');
Route::post('upduser', 'ApiController@updateUser');

Route::post('getcode', 'ApiController@getCode');
Route::post('resetpass', 'ApiController@validarCodigo');
Route::get('getresets', 'ApiController@getResets');

Route::post('enviarmensaje', 'ApiController@userMensaje');


Route::get('socialnetwork', 'ApiController@redirectToProvider');
Route::get('socialnetwork/callback', 'ApiController@handleProviderCallback');

//Route::post('google', ['middleware' => 'cors'userMensaje, 'uses' => 'ApiController@google']);
/*Route::group(['middleware' => 'cors'], function(Router $router){
    $router->post('google', 'ApiController@google');
});*/

Route::get('motores','ApiController@motores');
Route::get('categorias','ApiController@categorias');
Route::get('subcategorias','ApiController@subcategorias');

#API PARA aPP ANDROID
Route::get('almacenes','ApiController@almacenes');
Route::get('talleres','ApiController@talleres');
Route::get('servTalleres','ApiController@servTalleres');
Route::get('ofertas','ApiController@ofertas');
Route::get('autopartes','ApiController@autopartes');
Route::get('productos','ApiController@productos');
#Route::post('apilogin','ApiController@login');

Route::post('saveCompra', 'ApiController@saveCompra');
Route::get('getCompra', 'ApiController@getCompra');
Route::post('cart', 'ApiController@addCart');
Route::get('cart', 'ApiController@getCart');
Route::get('cartBadge', 'ApiController@getCartBadge');
Route::put('cart', 'ApiController@delItemCart');

Route::get('linea','ApiController@linea');
Route::get('marca','ApiController@marca');
Route::get('modelo','ApiController@modelo');
Route::get('departamento','ApiController@departamentos');
Route::get('ciudad','ApiController@ciudades');

#Taller controller
Route::resource('talleresc', 'TallerController');
Route::get('talleresc/{id}/delete', 'TallerController@destroy');

#User controller
Route::resource('user-system', 'UserController');
Route::get('user-system/{id}/delete', 'UserController@destroy');

#indexes
Route::get('indexes','controllerIndex@index');

#recuperar contraseña

Route::get("recuperarclave","loginController@recuperarclave");
Route::post("correoe","loginController@correo");

Route::get("forget","loginController@forget");

Route::post("guardarnewpw","loginController@guardarnewpw");
#searchs
Route::get('search','controllerIndex@search');

#tipotaller
Route::resource('tipo_taller',"TipoTallerController");
Route::get('tipo_taller/{id}/delete',"TipoTallerController@destroy");

#Servicio
Route::resource('servicio',"ServicioController");
Route::get('servicio/{id}/delete',"ServicioController@destroy");

#historico taller

//Route::resource('historico-taller',"HistoricoTallerController");
Route::get('historico-taller','HistoricoTallerController@historico_taller');

#historico Sesion
Route::get('historico-sesion',"HistoricoTallerController@historico_sesion");


#historico Usuario
//Route::resource('historico-usuario',"HistoricoTallerController");
Route::get('historico-usuario','HistoricoTallerController@historico_usuario');

