<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('/login', function(){

		return view ("layout_sign.layout_login");
});


Route::get('/principal', function(){
		$view="Menu Principal";
		$title="Principal";	
		return view ("index.index")->with(compact("view","title"));
});

Route::get('/perfil/inicio', function(){
		$view="Perfil";
		$title="Mi Perfil";	
		return view ("profile.index")->with(compact("view","title"));
});

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
