<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoSesion extends Model {

	protected $table = "historico_usuario";
	protected $fillable =[
							"nombre",
							"id_usuario",
							"correo",
	];


	public function getDetalles($fecha){

		return HistoricoSesion::select(\DB::raw("DATE_FORMAT(((created_at - INTERVAL 5 HOUR)),'%h:%i %p') as hora , correo,id_usuario,id"))->whereRaw("cast(created_at as date) = '".$fecha."'")->orderBy("id","desc")->get();
	}

	public function getUserType($id){

		echo "<strong>Usuario:   </strong>".HistoricoSesion::select("nombre")->find($id)->nombre."<strong>   Correo:   </strong>".HistoricoSesion::select("correo")->find($id)->correo;
		
		

		
		}

}
