<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Taller extends Model {

	//
 
 	protected $table="taller";

 	protected $fillable=[
 						"nombre",
 						"nombre_dueno",
 						"nombre_gerente",
 						"direccion",
 						"tipo_taller",
 						"telefono_fijo",
 						"telefono_movil",
 						"email",
 						"pagina_web",
 						"latitud",
 						"longitud",
 						"status"
 						];



 public function getServices($id){

 	return TallerServicio::where("id_taller","=",$id)->get();
 }

 public function getImagenes($id){

 	return TallerImagen::where("id_taller","=",$id)->get();
 }




}
