<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoTaller extends Model {

	protected $table="historico_taller";

	protected $fillable=[
 						"nombre",
 						"id_taller",
 						"nombre_dueno",
 						"nombre_gerente",
 						"direccion",
 						"tipo_taller",
 						"telefono_fijo",
 						"telefono_movil",
 						"email",
 						"pagina_web",
 						"latitud",
 						"longitud",
 						"status",
 						"id_creador",
 						"tipo_transaccion"
 						];

 						

	public function getDetalles($fecha){

		return HistoricoTaller::select(\DB::raw("DATE_FORMAT(((created_at - INTERVAL 5 HOUR)),'%h:%i %p') as hora , tipo_transaccion,id_taller,id"))->whereRaw("cast(created_at as date) = '".$fecha."'")->orderBy("id","desc")->get();
	}

	public function getUserType($id){

		echo HistoricoTaller::select("tipo_transaccion")->find($id)->tipo_transaccion."<strong>  =></strong>";
		echo "<strong>Usuario:   </strong>".HistoricoTaller::select("nombre")->find($id)->nombre."<strong>   Correo:   </strong>".HistoricoTaller::select("email")->find($id)->email;

		

		
		}

}
