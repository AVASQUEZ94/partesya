@extends('layout_sign.layout_principal_admin')

@section('counts')


@stop



@section('page')

<br>
	<div style="margin:0 auto; width:80%;">
        

    	<!-- usuarios -->    



    		<div class="panel panel-default">

			        <div class="panel-heading">

			          <h3 class="panel-title">SESIÓN</h3>

			        </div>

		        	<div class="panel-body">
		        	
		        		   <a href="{{asset('home')}}"   class="btn btn-success">Volver</a>
                		    <br><br>
		        		  
		        					<div class="panel panel-info">

								        <div class="panel-heading">

								          <h3 class="panel-title">HISTÓRICO</h3>

								        </div>

							        	<div class="panel-body">

							        	@if(Session::has('alert'))

											<div class="alert alert-warning alert-dismissable">

												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

												{{Session::get('alert')}}

											</div>

										@endif

							        		  
							        	<div style="margin:0 auto; width:80%">

							       
							        		<div class="col-sm-12" align="right">
							        		<div class="col-sm-6"></div>
								        	<div class="btn-group">
											  <a href="#" class="btn btn-default">ORDENAR POR</a>
											  <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown"></a>
											  <ul class="dropdown-menu">
											    <li><a href="{{asset('historico-sesion?order=desc')}}">Más Reciente</a></li>
											    <li><a href="{{asset('historico-sesion?order=asc')}}">Más Antiguo</a></li>
											  </ul>
											</div>
								        		
							        		</div>

										


							<table class="table table-striped table-hover ">
                                    <thead>
                                        <tr class="trblue">
                                            <th colspan="3">Histórico de Usuarios que Iniciarón Sesión</th>
                                    </thead>
                                    <tbody >

                                    @if(count($fechas)>0)

                                   @foreach($fechas as $fecha)

                                  	<tr>
                                  	<td colspan="3">
                                  		<legend><?php 
                                  		
                                  		$fecha_ingles  = $fecha->fecha_todas;
										$mesin = array("January", "February", "March","April","May", "June","July","August","September", "October","November","December");
										$mesen   = array("Enero", "Febrero", "Marzo","Abril","Mayo", "Junio","Julio","Agosto","Septiembre", "Octubre","Noviembre","Diciembre");

										$resultado = str_replace($mesin, $mesen, $fecha_ingles);
										
										$diasen = array("Sunday", "Monday", "Tuesday","Wednesday","Thursday", "Friday","Saturday");
										$diases   = array("Domingo", "Lunes", "Martes","Miércoles","Jueves", "Viernes","Sábado");

										echo $resultado2 = str_replace($diasen, $diases, $resultado);

                                  		 ?></legend>
                                  	</td>                                  		
                                  	</tr> 	

                                  		<?php $detalles=$fecha->getDetalles($fecha->fecha_dia); ?>
                                  		
                                  		   @if(count($detalles)>0)

			                                   @foreach($detalles as $detalle)

			                                  	<tr>
			                                  	<td> &nbsp;&nbsp; {{$detalle->hora}} &nbsp;&nbsp; 


			                                  	<img src="{{asset('img/ingresar.png')}}" width="30px">
			                                  	@if($detalle->id_usuario!=0)
			                                  	<?php $detalle->getUserType($detalle->id); ?>
			                                  	@else
			                                  	Todos
			                                  	@endif
			                                  
												</td>                                  	
			                                  	</tr> 	

			                                  		
			                                   @endforeach

			                                 @endif                                           
			                                    


                                   @endforeach

                                 @endif        


                                                                            
                                    
                                    </tbody>
                                </table>
						        				        								        		

							        	</div>



							             </div>

							   </div>


		             </div>

		   </div>
       


		     
  </div>


               

@stop