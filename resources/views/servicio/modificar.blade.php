@extends('layout_sign.layout_principal_admin')

@section('counts')


@stop



@section('page')






<div class="panel panel-default" style="margin:0 auto;width:60%">

              <div class="panel-heading">

                <h3 class="panel-title">Tipo de Taller</h3>

              </div>

              <div class="panel-body">

          @if(Session::has('alert'))

          <div class="alert alert-warning alert-dismissable">

          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

          {{Session::get('alert')}}
          </div>

        @endif



        @if(Session::has('message'))

        <div class="alert alert-dismissible alert-success">

        <button type="button" class="close" data-dismiss="alert">&times;</button>

         <strong>{{Session::get('message')}}</strong> </div>
                       

        @endif

        @if(count($errors)>0)

                  <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                     <ul>
                     @foreach($errors->all() as $error)
                         <li>{!!$error!!}</li>
                      
                        @endforeach
                        </ul>
                        </div>

          @endif
<br>


	<div style="margin:0 auto; width:80%;">
        


    		<div class="panel panel-info">

			        <div class="panel-heading">

			          <h3 class="panel-title">EDITAR TIPO DE Servicio</h3>

			        </div>

		        	<div class="panel-body">
  

		        		  
		       	

				<table class="table table-striped table-hover " style="margin:0 auto;width:60%">
                   <thead>
                    <a href="{{ asset('servicio') }}"  class="btn btn-success">Volver</a>
                          <br><br>
                        <tr class="trblue">
                           <th>Descripción</th>
                        </tr>
                   </thead>
                   <tbody>

                    <td>

                  {!!Form::model($doc,['route'=>['servicio.update',$doc->id], 'method' => 'PUT', 'class' => 'form'])!!}
                    <div class="col-sm-9">
                    {!! Form::text('descripcion',null,["class" =>"form-control","placeholder" =>"Nombre del registro"])!!}
                    
                    </div>

                     <div class="col-sm-3">
                      <button type="submit" class="btn btn-success">Guardar</button>
                     </div>

                   {!! Form::close() !!}    
                                            
                 </tbody> 

                 
            </table>                              		  
		        				

		             </div>



		   </div> 
       
		     
  </div>

  @yield('menu')



    

@stop