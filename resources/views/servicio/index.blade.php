@extends('layout_sign.layout_principal_admin')

@section('counts')


@stop



@section('page')






<div class="panel panel-default" style="margin:0 auto;width:60%">

              <div class="panel-heading">

                <h3 class="panel-title">Tipo de Servicio</h3>

              </div>

              <div class="panel-body">

          @if(Session::has('alert'))

          <div class="alert alert-warning alert-dismissable">

          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

          {{Session::get('alert')}}
          </div>

        @endif



        @if(Session::has('message'))

        <div class="alert alert-dismissible alert-success">

        <button type="button" class="close" data-dismiss="alert">&times;</button>

         <strong>{{Session::get('message')}}</strong> </div>

                    

        @endif  


           <table class="table table-striped table-hover " style="margin:0 auto;width:80%">
                   <thead>

                   <a href="{{ asset('home') }}"  class="btn btn-success">Volver</a>
                        <br><br>
                        <tr class="trblue">
                           <th>Descripción</th>
                           
                              <th> <div class="col-sm-15"><br>
                                         <a href="{{asset('servicio/create') }}" title="Nueva " id="Nuevo"><img src="{{asset('img/NEW.png')}}" width="30px">Agregar Nuevo</a>
                                   </div>
                                    </th>
                            </tr>
                   </thead>
                    <tbody>
                      @if(count($registros)>0)
                       @foreach($registros as $registro)

                       <tr>
                        <td>{{ $registro->descripcion }}</td>
                                                
                        <td>
                        <a href="{{ asset('servicio/'.$registro->id.'') }}">
                        <button class="btn btn-success">Modificar</button></a>
                <a href="{{ asset('servicio/'.$registro->id.'/delete') }}">
                        <button class="btn btn-danger">Eliminar</button></a>
                        </td>
                       </tr>

                       @endforeach
                     @endif
                      </tbody>
            </table>            
                    

                 </div>

       </div>
       
         
  </div>
         


    

@yield('menu')



    

@stop