  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
     
        <div class="pull-left image">
           <img src="{{asset('img/img_log_recortado.png')}}" class="img-circle" alt="User Image" >
                   </div>
        <div class="pull-left info">
        <br>        
          <a href="#"><i class="fa fa-circle text-success"></i> Conectado</a>
        </div>

      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Buscar...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU PRINCIPAL</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Opciones</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{asset('/')}}"><i class="fa fa-circle-o"></i> Inicio </a></li>
           

            @if(Auth::user()->tipo_usuario!==1)
               <li><a href="{{asset('/talleresc')}}"><i class="fa fa-circle-o"></i>Talleres</a></li>

               <li><a href="{{asset('/user-system')}}"><i class="fa fa-user"></i>Usuarios</a></li>

              @else
                <li><a href="{{asset('/talleresc')}}"><i class="fa fa-circle-o"></i>Talleres</a></li>

            @endif

          </ul>
        </li>

       @if(Auth::user()->tipo_usuario!==1)
      
           <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Administrar</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
               

               <li class=""><a href="{{asset('/tipo_taller')}}"><i class="fa fa-circle-o"></i> Tipo de Taller </a></li>


                <li><a href="{{asset('/servicio')}}"><i class="fa fa-circle-o"></i>Servicio</a></li>


              

              </ul>
            </li>





            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Histórico</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
               

               <li class=""><a href="{{asset('/historico-sesion')}}"><i class="fa fa-circle-o"></i> Sesión </a></li>


                <li><a href="{{asset('/historico-usuario')}}"><i class="fa fa-circle-o"></i>Usuario</a></li>


                <li><a href="{{asset('/historico-taller')}}"><i class="fa fa-circle-o"></i>Taller</a></li>

              

              </ul>
            </li>

      
            @endif

       <!-- <li>
          <a href="{{asset('/graficas')}}">
            <i class="fa fa-th"></i> <span>Graficas 1</span>            
          </a>
        </li>

        <li>
          <a href="{{asset('/graficas2')}}">
            <i class="fa fa-th"></i> <span>Graficas 2</span>            
          </a>
        </li> -->
     
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>