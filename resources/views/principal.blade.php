@extends('layout.layout')

@section('content')


<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    
        <table>
          <tr>
            <td>
             <img src="{{asset('img/partesyalogo.jpg')}}" width="60px" style="border-radius:10px" >  
            </td>
            <td>
              <a class="navbar-brand" href="{{asset('home')}}" style="margin-top:10px;"> <font color="" style="margin-left:15px;">PARTESYA</font></a>
            </td>
          </tr>
        </table>
         
  
    
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="#"  style="margin-top:20px;">
                                        {!!Auth::user()->name!!} /

                                         @if(Auth::user()->rol==1)Administrator @endif 

                                         @if(Auth::user()->rol==2)User System @endif 

                                         @if(Auth::user()->rol==3)County - User System @endif </a>
        </li>
      </ul>



      <ul class="nav navbar-nav">
      <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="margin-top:20px;">Administración<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{{asset('tipo_taller')}}">Tipo Taller</a></li>
            <li class="divider"></li>
            <li><a href="{{asset('servicio')}}">Servicio</a></li>
            <li class="divider"></li>
            <li><a href="#">Histórico</a>
                <ul><a href="{{asset('historico-sesion')}}">Sesión</a></ul>
                <ul><a href="{{asset('historico-usuario')}}">Usuario</a></ul>
                <ul><a href="{{asset('historico-taller')}}">Taller</a></ul>
                <ul><a href="#">Almacen</a></ul>
            </li>
            
            
          </ul>
        </li>
      
       </ul>
    
      <ul class="nav navbar-nav navbar-right">
                <li><a href="http://compuzoft.com/home/" target="_blank">Desarrollado por <img src="{{asset('img/logo.png')}}" width="100px" style="">  </a></li>
        <li><a href="{{asset('logout')}}" style="margin-top:20px;"><i class="fa fa-power-off "></i> Salir</a></li>
      </ul>
    </div>
  </div>
</nav>


@yield('menu')


@stop