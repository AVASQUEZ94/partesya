@extends('principal')

@section('menu')



<div style="margin:0 auto; width:97%;">



		<div class="panel panel-info">

			  <div class="panel-heading">

			    <h3 class="panel-title">INDEX -> <?php if(count($name)>0)	echo strtoupper($name[0]);?></h3>

			  </div>

			  <div class="panel-body">
			  <div class="col-sm-5">
			  <a href="#param">
			  <button class="btn btn-success">SEARCH INDEX</button>
				</a>
				<a href="{{asset('/search?full=true&in=')}}<?php echo $request->in;?>">
			  <button class="btn btn-success">FULL INDEX</button>
				</a>

				<a href="{{asset('/search?in=')}}<?php echo $request->in;?>">
			  <button class="btn btn-info">CLEAR SEARCH</button>
				</a>


				</div>

				<div  class="col-sm-5" id="fil_total"><?php  

				if(count($indexes)>0){
					$aux=$indexes->currentPage()-1;
					
					if($indexes->currentPage()==1){
						$inicial=1;
						$final=count($indexes);
					}
					else
						if($indexes->currentPage()==$indexes->LastPage()){
						$inicial=(($indexes->currentPage()-1)*100)+1;
						$final=$indexes->total();

						}
					else{
						$inicial=($indexes->currentPage()-1)*100+1;
						$final=($indexes->currentPage())*100;
					}
						

					echo "Page ".$indexes->currentPage()." Showing from ".$inicial." to ".$final." of ".$indexes->total();


										}
				 ?>
				 	
				</div>
		
<br><br>

			<div class="tablescroll">
				<table class="table table-striped table-hover  " id="dataTables-example">
					  <thead>
					    <tr style="background-color:#5bc0de; color:#fff;cursor: pointer; ">
					      <th>GRANTOR</th>
					      <th>GRANTEE</th>
					      <th>PROPERTY DESC</th>
					      <th>BOOK</th>
					      <th>VOLUME</th>
					      <th>PAGE</th>
					      <th>INST.DATE</th>
					      <th>FILED.DATE</th>
					      <th>DOC.TYPE</th>
					      <th>PREVIEW</th>
					    </tr>
					  </thead>
				
					  <tbody >
					    @if(count($indexes)>0)

                                 <?php  $numeracion=0;


                                 ?>



          	            @foreach($indexes as $index)


          	            	<?php 

          	            	 /*$cole=$index->getRuta($index->uniquekey); 

          	            	 
	          	            	$info=$cole->toArray();

	          	            	if(count($info)>0){
	          	            	$url=$info[0]["web_path"];
	          	            	$url=str_replace("E:/", "../../", $url);
	          	           		 }
	          	            	else*/
	          	            	$url=$index->web_path;
	          	           		 $url=str_replace("E:/", "../../", $url);
          	            	
          	          
          	            	?>
                                          <tr>
                                            <td>{{$index->grantor}}</td>
                                            <td>{{$index->grantee}} </td>
                                            <td>  {{$index->reference}} </td>
	                                      <td>
											{{$index->book}}
											</td>
                                          <td>
											{{$index->volume}}
											</td>
                                      
                                      	<td>
                                      		{{$index->page}}
                                      	</td>

                                      	  <td>
                                      	  {{$index->instrument_date}}
											
											</td>
                                      	<td>
                                      		 {{$index->field_date}}
                                      	</td>
                                      	<td>
                                      		{{$index->document_type}}
                                      	</td>
                                      	<td>
                                      	<a href="#popup1"><button class=" btn btn-info prev" value='<?php echo $url;?>'>Preview</button></a>
                                    
                                      		
                                      	</td>
                                      
                                      	 </tr>
  	 

                           @endforeach

                          
                        @endif
					
				
					  </tbody>

					</table> 
 </div>


		 @if(count($indexes)>0)
			<?php $_SESSION['pageCurrent']=$indexes->currentPage(); $_SESSION['lastPage']=$indexes->LastPage();  ?> 

			    {!!

			    $indexes
			    ->appends(['in' => $request->in])
			    ->appends(['vol' => $request->vol])
			    ->appends(['pag' => $request->pag])
			    ->appends(['grantee' => $request->grantee])
			    ->appends(['grantor' => $request->grantor])
			    ->appends(['doctype' => $request->doctype])
			    ->appends(['propdec' => $request->propdec])
			    ->appends(['docnumber' => $request->docnumber])
			   	->appends(['full' => true])
			    ->render()
			  
			    !!}
		 @endif			


			  </div>

		</div>


</div>



<div id="popup1" class="overlay">
	<div class="popup">
		<h2>Document</h2>
		<a class="close" href="#">&times;</a>
		<div class="content tablescroll">

		<iframe src="" width="95%" id="ifrma" height="680px"></iframe>

		</div>
		
	</div>
</div>



<div id="param" class="overlay">
	<div class="popup">
		<h2>Parameters</h2>
		<legend>From 01/01/1998 To 06/06/2016</legend>
		<a class="close" href="#">&times;</a>
		<div class="content">
		<br>
				<form action="search" method="GET">
			


			<div class="col-sm-12">

				<div class="col-sm-3">
				County
			<select name="in" class="form-control">
			
			  @if(count($inds)>0)

                             
          	            @foreach($inds as $index)


          	     	<option value="<?php  echo $index->id_index ?>" @if($request->in==$index->id_index) selected @endif>{{$index->name}}</option>
						  	 

                           @endforeach

                         @endif	
			</select>
			</div>


			<div class="col-sm-3">
			Volume
				<input type="text" class="form-control" id="volumec" name="vol" value="<?php echo $request->vol ?>" placeholder="Volume">

				
			</div>

			<div class="col-sm-3">
			Page
				<input type="text" class="form-control" id="pagec" name="pag" value="<?php echo $request->pag ?>" placeholder="Page">

				
			</div>
			<div class="col-sm-3 has-success">
			Grantee
				<input type="text" class="form-control" name="grantee" id="granteec" value="<?php echo $request->grantee ?>" placeholder="Grantee">

				
			</div>

			</div>


			
			<div class="col-sm-12">


			<div class="col-sm-3 has-success">
			<br>
			Grantor
				<input type="text" class="form-control" name="grantor" id="grantorc" value="<?php echo $request->grantor ?>" placeholder="Grantor">

				
			</div>
				<div class="col-sm-3">
				<br>
				Doc Type
				<input type="text" class="form-control" name="doctype" id="doctypec" value="<?php echo $request->doctype ?>" placeholder="Doc Type">

				
			</div>

			<div class="col-sm-3">
				<br>
			Document Number
				<input type="text" class="form-control" name="docnumber" id="docnumc" value="<?php echo $request->docnumber ?>" placeholder="Document number">

				
			</div>
			

			<div class="col-sm-3">
				<br>
				Property Desc
				<input type="text" class="form-control" name="propdec" id="propdec" value="<?php echo $request->propdec ?>" placeholder="Property desc">

				
			</div>



			 </div>

			<!--<div class="col-sm-12">


			<div class="col-sm-12">
			<br>
			Book
			<select class="form-control" name="book">
				<option>Select</option>
			</select>
			
			</div>
				
			 </div> -->

			 	<div class="col-sm-12">


			<div class="col-sm-6">
			<br>
			From select filed date
				<input type="date" class="form-control" name="fromsfd" value="">

				
			</div>
				<div class="col-sm-6">
				<br>
				To select filed date
				<input type="date" class="form-control" name="tosfd" value="">

				
			</div>


			 </div>


			 		<div class="col-sm-12">


			<div class="col-sm-6">
			<br>
			From select instrument date
				<input type="date" class="form-control" name="fromsid" value="">

				
			</div>
				<div class="col-sm-6">
				<br>
				To select instrument date
				<input type="date" class="form-control" name="tosid" value="">

				
			</div>


			 </div>

				 <div class="col-sm-12 form-group">
					 <div class="col-sm-6">
					 <br>
					<input type="submit" class="btn btn-info" id="searchRescordss" name="" value="Search Records">
				</div>

				<div class="col-sm-6">
					 <br>
					<input type="reset" class="btn btn-info" id="cleanfields" name="" value="Clear Fields">
				</div>
					</div>
			</form>
	
					
			<div class="col-sm-12 form-group" >
				<div class="" >
				<center>
					<div id="cont-img" style="display:none">
					<img src="103.gif" id="imggif"  style=""><br>Searching information
					</div>
					</center>
				</div>
			</div>
		</div>
		
	</div>
</div> 

<script type="text/javascript">
	
$("#cleanfields").click(function(){

	$("#volumec").val("");
	$("#pagec").val("");
	$("#grantorc").val("");
	$("#granteec").val("");
	$("#doctypec").val("");
	$("#docnumc").val("");
	$("#propdec").val("");


});

</script>

@stop



