@extends('layout_sign.layout_principal_admin')

@section('counts')


@stop



@section('page')


<br>
<div style="margin:0 auto; width:90%;">


    <div class="panel panel-default">

        <div class="panel-heading">

          <h3 class="panel-title">USUARIOS -> LISTA</h3>

        </div>

        <div class="panel-body">
@if(count($errors)>0)

                  <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                     <ul>
                     @foreach($errors->all() as $error)
                         <li>{!!$error!!}</li>
                      
                        @endforeach
                        </ul>
                        </div>

                    @endif

                    @if(Session::has('message-error'))

                  <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                       {{Session::get('message-error')}}
                        </div>

                    @endif


                     @if(Session::has('message'))

                    <div class="alert alert-success alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                         {{Session::get('message')}}
                          </div>

                      @endif


                        <div class="tooltip-demo">
                        <a href="{{url('user-system/create')}}">

                         <button class="btn btn-success adduser" data-toggle="tooltip" data-placement="right" title="Click to add a new user"> 
                        
                         
                            AGREGAR USUARIO !

                        </button> </a>
                        </div>
                       <br>


            
            {!! Form::open(['url' => 'user-system', 'method' => 'GET']) !!}
   
              <div class="col-lg-4"> 

                
                <div class="form-group tooltip-demo"> 
                   {!!Form::text('s_name',$request->s_name,[ 'class' =>"form-control", 'placeholder' => 'Buscar por nombre' , 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Buscar por nombre de usuario'])!!}
                </div>


              </div>


              <div class="col-lg-4"> 

                  <div class="form-group tooltip-demo"> 
                    {!!Form::text('s_email',$request->s_email,[ 'class' =>"form-control", 'placeholder' => 'Buscar por correo','autofocus'=>'true', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Buscar por correo: example@domain.com'])!!}
                  </div>

              </div>


              <div class="col-lg-3"> 

                 <div class="form-group tooltip-demo"> 
                    <select name="status" class="form-control"   data-toggle="tooltip" data-placement="top" title="Filter by active users or inactive users">
                        <option value="active" @if($status=="active")selected @endif >Usuarios Activos</option>
                        <option value="inactive" @if($status=="inactive")selected @endif>Usuarios Inactivos</option>
                    </select>
                 </div>

              </div>


            <div class="col-lg-1"> 

                 <div class="form-group tooltip-demo"> 
                      <button type="submit" class="form-control"  data-toggle="tooltip" data-placement="top" title="Clic para Buscar"><i class="fa fa-search fa-1x"></i></button>
                 </div>

            </div>


      {!!Form::close()!!}



        <div class="tablescroll"> 

                  <div class="table-responsives">
                                <table class="table table-striped table-hover ">
                                    <thead>
                                        <tr class="trblue">
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Tipo de Usuario</th>
                                            <th>Email (Nombre de Usuario)</th>
                                            <th>Teléfono</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>


                        @if(count($users)>0)

                                 <?php $numeracion=0;?>



          	            @foreach($users as $user)

                                          <tr>
                                            <td><?php echo ++$numeracion;?></td>
                                            <td>{{$user->name}} </td>
                                            <td><?php
                                                       if($user->rol==1)echo "Administrador"; 
                                                       else
                                                         if($user->rol==2)echo "Usuario del Sistema"; 
                                                       else
                                                         if($user->rol==3)echo "Cliente del Sistema"; 

                                                       ?></td>
                                            <td>{{$user->email}} </td>
                                            <td>{{$user->movil}} </td>
                                            <td>
                                            <div class="tooltip-demo">
                                            <a href="{{url('user-system/'.$user->id.'/edit')}}">
                                           {!!Form::button('<i class="fa fa-search fa-1x"></i> Detalles',['class' =>"form-control btn btn-info", 'data-toggle'=>'tooltip','data-placement'=>'left','title'=>'Ver detalles de '.$user->name.''])!!}</a>
                                           </div>
                                           </td>
                                      			  </tr>
  	 

                           @endforeach

                             @else 
                                No records found

                        @endif
                                      
                                    
                                    </tbody>
                                </table>
                            </div>
         

    </div>  <?php $_SESSION['pageCurrent']=$users->currentPage(); $_SESSION['lastPage']=$users->LastPage();  ?> 

    {!!$users->render()!!}





 


                        </div>
                        <div class="panel-footer">
                           Clic Sobre el boton detalles para editar.
                        </div>
                    </div>
                  
           
            </div>


                    <!-- /.col-lg-4    ,'style' =>"background-color:#040B1F;"              -->
               


@stop