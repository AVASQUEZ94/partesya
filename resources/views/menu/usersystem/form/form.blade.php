
	 <div class="col-sm-12">
 	<legend><i>Datos Personales</i></legend>
 </div>

	<div class="col-lg-6"> 

		<div class="form-group tooltip-demo has-success"> 
			<b> {!!Form::label('NOMBRES Y APELLIDOS')!!}</b> 	<font style="color:red">*</font>

			{!!Form::text('name',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Ingrese un nombre', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo obligatorio'])!!}
		</div>
	
	</div>


	<div class="col-lg-6"> 

		<div class="form-group tooltip-demo"> 

		{!!Form::label('TELÉFONO')!!}

		{!!Form::text('movil',null,['class'=>'form-control' , 'maxlength'=>'30','placeholder'=>'ingrese su número de teléfono' , 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo opcional'])!!}

		</div>

	</div>


	<div class="col-lg-12"> 

		<div class="form-group tooltip-demo"> 
			{!!Form::label('DIRECCIÓN')!!}

			{!!Form::text('address',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Ingrese una dirección', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo opcional'])!!}
		</div>

	</div>


 <div class="col-sm-12">
 	<legend><i>Datos de Usuario</i></legend>
 </div>

	<div class="col-lg-6"> 

		<div class="form-group tooltip-demo has-success"> 
			<b>{!!Form::label('EMAIL')!!}</b>	 	<font style="color:red">*</font>

			{!!Form::text('email',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Usaras este email para iniciar sesión', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo obligatorio'])!!}
		</div>

	</div>




	<div class="col-lg-6"> 

		<div class="form-group tooltip-demo has-success"> 
			<b>{!!Form::label('CONTRASEÑA')!!}</b>	 <font style="color:red">*</font>

			{!!Form::password('password',['class'=>'form-control', 'maxlength'=>'20', 'placeholder'=>'Ingrese una contraseña', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo obligatorio'])!!}
		</div>

	</div>


  @if (Auth::check())        

	<div class="col-lg-12"> 

		<div class="form-group tooltip-demo has-success"> 

			<b>{!!Form::label('PERFIL')!!}</b>	    <font style="color:red">*</font>

			<?php 
				$array=Array();
				if(Auth::user()->rol==1) 
					$array=Array(1 => "Administrador", 2 => "Usuario del sistema P/Carga de talleres", 4 => "Usuario del sistema P/Carga de productos", 3 =>"Cliente del sistema");
				else
					$array=Array(2 => "Usuario del sistema");
			

				?>
			{!!Form::select('rol', $array,null,['class'=>'form-control', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo obligatorio'])!!}
		
		</div>

	</div>

 @endif
 <div class="col-sm-12">
 	<legend><i>Datos de Vehiculo</i></legend>
 </div>

 <div class="col-lg-4"> 

		<div class="form-group tooltip-demo"> 

			{!!Form::label('MARCA')!!}

			<select name="marca_v" class="form-control">
				<option value="0">-- Seleccione --</option>
			</select>
		
		</div>

</div>


 <div class="col-lg-4"> 

		<div class="form-group tooltip-demo"> 

			{!!Form::label('MODELO')!!}

			<select name="modelo_v"  class="form-control">
				<option value="0">-- Seleccione --</option>
			</select>
		
		</div>

</div>


 <div class="col-lg-4"> 

		<div class="form-group tooltip-demo"> 

			{!!Form::label('AÑO')!!}

			<select name="año_v"  class="form-control">
				<option value="0">-- Seleccione --</option>
			</select>
		
		</div>

</div>