@extends('layout_sign.layout_principal_admin')

@section('counts')


@stop



@section('page')

<br>
<div style="margin:0 auto; width:80%;">


    <div class="panel panel-default">

        <div class="panel-heading">

          <h3 class="panel-title">USUARIOS -> EDITAR</h3>

        </div>

        <div class="panel-body">

                            @if(count($errors)>0)

                  <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                     <ul>
                     @foreach($errors->all() as $error)
                         <li>{!!$error!!}</li>
                      
                        @endforeach
                        </ul>
                        </div>

                    @endif

                    @if(Session::has('message-error'))

                  <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                       {{Session::get('message-error')}}
                        </div>

                    @endif


                     @if(Session::has('message'))

                    <div class="alert alert-success alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                         {{Session::get('message')}}
                          </div>

                      @endif
	
                                {!!Form::model($user,['route'=>['user-system.update',$user->id], 'method' => 'PUT', 'class' => 'form'])!!}

	
                                @include('menu.usersystem.form.form')


                                @if(Auth::user()->rol==1)
                                  <div class="col-lg-4"> 

                                     <div class="form-group"> 

                                         {!!Form::checkbox('status', 'true',  $user->status )!!} Active 
 
                                       </div>



                                  </div>
                                  @endif

  	<br>

	                               <div class="col-lg-12 botons-form tooltip-demo" > 


                                      {!!Form::button(' <i class="fa fa-edit fa-1x"></i>  ACTUALIZAR',[ 'type'=>'submit',  'class' => 'btn btn-success' ,'autofocus'=>'true', 'style' => 'margin-right: 15px;', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'After modify the user press this buttom'])!!}     

                                       @if(Auth::user()->rol==1)
                                        <a href="{{url('user-system/'.$user->id.'/delete')}}">{!!Form::button('  <i class="fa fa-times fa-1x"></i>   ELIMINAR', ['class' => 'btn btn-danger',  'style' => 'margin-right: 15px;', 'onclick' => "return confirm('You really want to delete this user?');", 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Delete this user'])!!}</a>
                                        @endif

                                         @if(Auth::user()->rol==1)

                                        <a href="{{url('user-system')}}">
                                        @else
                                         <a href="{{url('home')}}">
                                        @endif
                                        {!!Form::button(' <i class="fa fa-arrow-circle-o-left fa-1x"></i> CANCELAR',['class' => 'btn btn-primary' , 'style' => 'margin-right: 15px;', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Go back' ])!!}</a>
 

                                        {!!Form::button('<i class="fa fa-refresh fa-1x"></i> LIMPIAR',[ 'type'=> 'reset', 'class' => 'btn btn-primary' , 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Refresh'])!!}     


	                                 </div>

	
                {!!Form::close()!!}

                        </div>
                        <div class="panel-footer">
                            Por favor complete todos los campos con   <font class="color-red">*</font>    </div>
                    </div>
                  
           
            </div>





@stop