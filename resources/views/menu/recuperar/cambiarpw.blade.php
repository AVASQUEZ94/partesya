@extends('layout.layout')

@section('js')
<script type="text/javascript" src="{{asset('js/validatelogin.js')}}"></script>
@stop

@section('content')

<div style="margin:0 auto; width:40%; margin-top:5%">


<div class="panel panel-info">

	
	  <div class="panel-body">
	  		@if(Session::has('alert'))
	<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{{Session::get('alert')}}
		</div>
	@endif

	@if(Session::has('message-error'))

<div class="alert alert-warning alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     {{Session::get('message-error')}}
      </div>

	@endif

			@if(Session::has('message'))

							<div class="alert alert-dismissible alert-success">

							  <button type="button" class="close" data-dismiss="alert">&times;</button>

							  <strong>{{Session::get('message')}}</strong> </div>

										

							@endif
<div class="col-sm-12">

	<div class="col-sm-12">
	<legend>Recuperación de Contraseña</legend>

		<form action="{{asset('guardarnewpw')}}" method="post" onsubmit="return validar()">
		<input type="hidden" name="_token" value="{{csrf_token()}}"></input>
			<div class="form-group" align="justify" id="nuevacontra">
			<input type="hidden" name="userid" value="{{$recup}}" >
				<label>Nueva Contraseña</label>
				<input type="password" class="form-control" name="password" placeholder="Nueva Contraseña" autocomplete="off">
				
			</div>
			<div class="form-group" align="justify" id="repetircontra">
				<label>Repetir Contraseña</label>
				<input type="password" class="form-control" name="password2" placeholder="Repetir Contraseña" autocomplete="off">
				<div >* Ultimo paso. Ingrese su nueva contraseña para acceder al portal nuevamente, el enlace es válido solo por 10 minutos.</div>
			</div>

			<div class="form-group">
			<br><br>
				<input type="submit" name="" class="btn btn-success" value="Guardar">
				<a href="{{asset('principal')}}" class="btn btn-info">Volver</a>
			</div>

		</form>

</div>


<div class="col-sm-12">

	<center>
	    	<img src="{{asset('img/partesyalogo.jpg')}}"  width="40%">
	    </center>
</div>
	
</div>


	  </div>

</div>



</div>

@stop