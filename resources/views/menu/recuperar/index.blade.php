@extends('layout.layout')

@section('content')

<div style="margin:0 auto; width:40%; margin-top:5%">


<div class="panel panel-info">

	
	  <div class="panel-body">
	  		@if(Session::has('alert'))
	<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{{Session::get('alert')}}
		</div>
	@endif

	@if(Session::has('message-error'))

<div class="alert alert-warning alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     {{Session::get('message-error')}}
      </div>

	@endif

			@if(Session::has('message'))

							<div class="alert alert-dismissible alert-success">

							  <button type="button" class="close" data-dismiss="alert">&times;</button>

							  <strong>{{Session::get('message')}}</strong> </div>

										

							@endif
<div class="col-sm-12">

	<div class="col-sm-12">
	<legend>Recuperar Contraseña</legend>

		<form action="{{asset('correoe')}}" method="post">
		<input type="hidden" name="_token" value="{{csrf_token()}}"></input>
			<div class="form-group">

				<label>Usuario</label>
				<input type="text" class="form-control" name="username" placeholder="Nombre de usuario">
				<div><b>Nota:</b> Ingrese su nombre de usuario, luego se le enviara un correo y con el enlace adjunto podra recuperar su contraseña.</div>
			</div>

			<div class="form-group">
			<br>
				<input type="submit" name="" class="btn btn-success" value="Recuperar">
				<a href="{{asset('/')}}" class="btn btn-info">Volver</a>
			</div>

		</form>

</div>


<div class="col-sm-12">

	<center>
	    	<img src="{{asset('img/partesyalogo.jpg')}}" width="40%">
	    </center>
</div>
	
</div>


	  </div>

</div>



</div>

@stop