@extends('layout_sign.layout_principal_admin')

@section("js")
<script type="text/javascript">

  $("#addservicio").click(function(e){
    var x = document.getElementsByName("servicioofrecido[]");
    var a=true;
    for (var i = 0; i < x.length; i++) {
        if(x[i].value == $("select[name=serviciopre]").val())
          a=false;
    }
    if(a){
        $("#divtable").fadeIn();
        $("#divtbody").append("<tr><td width='35px'><a href='#servicios' onclick='alert(1)' style='padding: 3px 8px;' class='btn btn-danger' title='Borrar Servicio:  "+$("select[name=serviciopre] option:selected").text()+"'>x</a><input type='hidden' name='servicioofrecido[]' value='"+$("select[name=serviciopre]").val()+"'></td><td>"+$("select[name=serviciopre] option:selected").text()+"</td></tr>");   }


  });
  </script>


      <script type="text/javascript">
  function remove(a){
     a.closest('tr').remove();
  }

    </script>

    <script type="text/javascript" >
    
    function colocarposition(coor){
        $("input[name=latitud]").val(coor.lat);
        $("input[name=longitud]").val(coor.lng);
    }

    </script>
<style type="text/css">
   #map {
        height: 500px;
      }

</style>

 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAY1lmayfNlau6JCMI5h78HEVbnBn5T2WQ&libraries=places&callback=initMap"
        async defer></script>

@stop

@section('page')

<br>
<div style="margin:0 auto; width:80%;">


    <div class="panel panel-default">

        <div class="panel-heading">

          <h3 class="panel-title">TALLER -> EDITAR</h3>

        </div>

        <div class="panel-body">

                            @if(count($errors)>0)

                  <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                     <ul>
                     @foreach($errors->all() as $error)
                         <li>{!!$error!!}</li>
                      
                        @endforeach
                        </ul>
                        </div>

                    @endif

                    @if(Session::has('message-error'))

                  <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                       {{Session::get('message-error')}}
                        </div>

                    @endif


                     @if(Session::has('message'))

                    <div class="alert alert-success alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                         {{Session::get('message')}}
                          </div>

                      @endif
	
                    


                                                       <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-home"></i> Index</a></li>
              <li><a href="#tab_2" data-toggle="tab"><i class="ion ion-clipboard"></i> Servicios</a></li>   
              <li><a href="#tab_3" data-toggle="tab"><i class="ion ion-clipboard"></i> Tipo Taller</a></li>

              <li><a href="#tab_4" data-toggle="tab"><i class="fa fa-camera"></i> Imágenes</a></li>

              <li><a href="#tab_5" data-toggle="tab"><i class="fa fa-map-marker"></i> Localización</a></li>

            
              
            </ul>
            <div class="tab-content">
             
              <div class="tab-pane active" id="tab_1">

                    
                    <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Datos del Taller</h3>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
            
                    <div class="box-body">

                    <div class="col-sm-12" id="response_m">
                      
                    </div>

                      

                  {!!Form::model($user,['route'=>['talleresc.update',$user->id], 'method' => 'PUT', 'class' => 'form',"enctype" => "multipart/form-data"])!!}

  
                        @include('menu.taller.form.form')

                            <div class="col-lg-4"> 
                                  <br><br>
                                     <div class="form-group"> 

                                         {!!Form::checkbox('status', 'true',  $user->status )!!} Activo
 
                                       </div>



                                  </div>


                 <div class="col-sm-12"><strong>Nota:</strong> Los campos con * son requeridos.</div>

                                        

                    </div>
                    <!-- /.box-body -->

                     <div class="box-footer">

                           <center>

                                 <div class="col-lg-12 botons-form tooltip-demo" > 


                                      {!!Form::button(' <i class="fa fa-edit fa-1x"></i>  ACTUALIZAR',[ 'type'=>'submit',  'class' => 'btn btn-success' ,'autofocus'=>'true', 'style' => 'margin-right: 15px;', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Despues de modificar los campos presiona este boton'])!!}     

                                        <a href="{{url('talleresc/'.$user->id.'/delete')}}">{!!Form::button('  <i class="fa fa-times fa-1x"></i>   ELIMINAR', ['class' => 'btn btn-danger',  'style' => 'margin-right: 15px;', 'onclick' => "return confirm('Realmente quieres eliminar este taller?');", 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Borrar este taller'])!!}</a>

                                        <a href="{{url('talleresc')}}">{!!Form::button(' <i class="fa fa-arrow-circle-o-left fa-1x"></i> ATRAS',['class' => 'btn btn-primary' , 'style' => 'margin-right: 15px;', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Atras' ])!!}</a>
 

                                        {!!Form::button('<i class="fa fa-refresh fa-1x"></i> LIMPIAR',[ 'type'=> 'reset', 'class' => 'btn btn-primary' , 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'Refresh'])!!}     


                                   </div>
                                    </center>



                    </div>
                    
               
                </div>

                  

              </div>

              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">


                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Servicios Ofrecidos</h3>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
            
                    <div class="box-body">

                    
                     @include('menu.taller.form.form2')

                                  

                    </div>
                    <!-- /.box-body -->

               
                </div>

              </div>

                <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">


                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Tipo de Taller</h3>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
            
                    <div class="box-body">

                    
                     @include('menu.taller.form.form3')

                                  

                    </div>
                    <!-- /.box-body -->

               
                </div>

              </div>
              
              

                 <div class="tab-pane" id="tab_4">


                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Imágenes</h3>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
            
                    <div class="box-body">

                    
                     @include('menu.taller.form.form4')

                                  

                    </div>
                    <!-- /.box-body -->

               
                </div>

              </div>



                <div class="tab-pane" id="tab_5">


                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Localización</h3>
                  </div>
                  <!-- /.box-header -->
                  <!-- form start -->
            
                    <div class="box-body">

                    
                     @include('menu.taller.form.form5')

                                  

                    </div>
                    <!-- /.box-body -->

               
                </div>

              </div>

            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->


                  {!!Form::close()!!}

                        </div>
                        <div class="panel-footer">
                            Por favor complete todos los campos con   <font class="color-red">*</font>    </div>
                    </div>
                  
           
            </div>





@stop