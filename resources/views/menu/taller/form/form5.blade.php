
 <div class="col-sm-12">
 <br>

 	<legend><i>Datos de Localización</i></legend>
 	<br>
<div class="col-lg-6"> 

		<div class="form-group tooltip-demo has-success"> 
			{!!Form::label('DIRECCIÓN')!!} <font style="color:red">*</font>

			</div>

	</div>
 	<br>

 	@if(isset($user->id))
 	{!!Form::hidden('latitud',null)!!}

 	{!!Form::hidden('longitud',null)!!}
 	@else
 	<input type="hidden" name="latitud" value="10.997901751041832">
 	<input type="hidden" name="longitud" value="-74.80422703750003" >
 	
 	@endif
 	<div class="col-sm-12">
 	<!--	Por favor elija la coordenada del taller.-->

 	<div class="col-sm-6 has-success">
    {!!Form::text('direccion',null,['class'=>'form-control', 'maxlength'=>'200', 'placeholder'=>'Dirección del taller', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo obligatorio',"id" =>"address"])!!}
  </div>
      <input id="submit" type="button" value="Buscar Dirección" class="btn btn-info">
    
      <br>
    <div id="map"></div>

   <script>
      var markers = [];

function initMap() {

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 14,
    center: {lat: 10.997901751041832, lng: -74.80422703750003}
  });
  var geocoder = new google.maps.Geocoder();

  document.getElementById('submit').addEventListener('click', function() {
      
    geocodeAddress(geocoder, map);
  });
}

function geocodeAddress(geocoder, resultsMap) {
  var address = document.getElementById('address').value;
  geocoder.geocode({'address': address}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      setMapOnAll(null);
      markers = [];
      colocarposition(results[0].geometry.location);
      resultsMap.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
        map: resultsMap,
        position: results[0].geometry.location
      });
        markers.push(marker);


    } else {
      
    }
  });
}

function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

    </script>


 	</div>
 </div>

