
 <div class="col-sm-6">
 	<legend><i>Tipo de Taller</i></legend>

 	<div class="col-sm-12">
 		
 		<select class="form-control" name="tipo_taller">
 			
 			@if(count($tipotaller))
 				
 				@foreach($tipotaller as $tipota)
 				<option value="{{$tipota->id}}" @if(isset($user->id)) @if($tipota->id==$user->tipo_taller) selected @endif     @endif>{{$tipota->descripcion}}</option>
 				@endforeach

 			@endif


 		</select>
 	</div>
  </div>