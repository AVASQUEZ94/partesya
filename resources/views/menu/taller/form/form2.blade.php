	
 <div class="col-sm-6">
 	<legend id="services"><i>Servicios Prestados</i></legend>

 	<div class="col-sm-12">
 		
 	<div class="col-sm-9">
 		<select class="form-control" name="serviciopre">
 			@if(count($servis))
 				
 				@foreach($servis as $serv)
 				<option value="{{$serv->id}}">{{$serv->descripcion}}</option>
 				@endforeach
 			@endif
 		</select>
 	</div>

 	<div class="col-sm-3">
 	<a href="javascript:void(0)" class="btn btn-success" id="addservicio">AGREGAR</a>
 	</div>
 	</div>





 	<div class="col-sm-12">
 	<br>
 		<table id="divtable" >
 		<thead>
 			<tr>
 				<th width='35px'></th>
 				<th>Nombre del Servicio</th>
 			</tr>
 		</thead>
 		@if(isset($user->id))

 				<?php $services=$user->getServices($user->id);

 				 ?>

 				 @foreach($services as $service)

 				 	<tr>
	 				<td width='35px'><a href='#servicios' onclick='remove($(this))' style='padding: 3px 8px;' class='btn btn-danger' title='Borrar Servicio: {{$service->getnameServicio($service->id_servicio)}} '>x</a><input type='hidden' name='servicioofrecido[]' value='{{$service->id_servicio}}'></td><td>{{$service->getnameServicio($service->id_servicio)}}</td></tr>

 				 @endforeach


		 	@endif
 		<tbody id="divtbody">

 		</tbody>
 	</table>
 	</div>
 	

 </div>