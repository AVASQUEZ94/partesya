<style type="text/css">
.btn-file {
  position: relative;
  overflow: hidden;
  }
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
</style>
<div class="col-sm-6">

	<div class="col-sm-12">
	<br>
		<legend><i>Imágen de Perfil</i></legend>


           <span class="btn btn-success btn-file">
          
                <i class="fa fa-camera"></i>   CARGAR FOTO  <input type="file" name="imag_1" >

                </span>
        

	</div>

</div>

 <div class="col-sm-6">
 <br>
 	<legend><i>Imágenes del Taller</i></legend>

 	<div class="col-sm-12">

 	          <span class="btn btn-success btn-file">
          
                <i class="fa fa-camera"></i>   CARGAR IMÁGENES  <input type="file" name="imagenes[]"  multiple="multiple">

                </span>
 		<br>
 		Nota: Si cargas nuevas imagenes se borraran las anteriores y se guardaran solamente las nuevas.
 		<br>

 		<br>
 	</div>

 	<div class="col-sm-12">
 	@if(isset($user->id))
 	
 		<?php $imgs=$user->getImagenes($user->id); ?>

 		@foreach($imgs as $img)

 			<img src="{{asset('taller/gallery/'.$img->file_name.'')}}" alt="Error cargar imagen" width="150px" height="100px">

 		@endforeach
 	
 	@endif
 	</div>

 </div>