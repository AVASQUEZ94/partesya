
	<div class="col-lg-6"> 

		<div class="form-group tooltip-demo has-success"> 
			<b> {!!Form::label('NOMBRE DEL TALLER')!!}</b> 	<font style="color:red">*</font>

			{!!Form::text('nombre',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Ingrese el nombre del taller', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo obligatorio'])!!}
		</div>
	
	</div>

	<div class="col-lg-3"> 

		<div class="form-group tooltip-demo has-success"> 

		{!!Form::label('NOMBRES DEL DUEÑO')!!} <font style="color:red">*</font>

		{!!Form::text('nombre_dueno',null,['class'=>'form-control' , 'maxlength'=>'50','placeholder'=>'Nombres y Apellidos' , 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo obligatorio'])!!}

		</div>

	</div>

	<div class="col-lg-3"> 

		<div class="form-group tooltip-demo"> 

		{!!Form::label('NOMBRES DEL GERENTE')!!}

		{!!Form::text('nombre_gerente',null,['class'=>'form-control' , 'maxlength'=>'30','placeholder'=>'Nombres y Apellidos' , 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo opcional'])!!}

		</div>

	</div>

	<div class="col-lg-3"> 

		<div class="form-group tooltip-demo"> 

		{!!Form::label('TELÉFONO FIJO')!!}

		{!!Form::text('telefono_fijo',null,['class'=>'form-control' , 'maxlength'=>'11','placeholder'=>'ingrese el número de teléfono fijo' , 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo opcional'])!!}

		</div>

	</div>

		<div class="col-lg-3"> 

		<div class="form-group tooltip-demo"> 

		{!!Form::label('TELÉFONO MOVIL')!!}

		{!!Form::text('telefono_movil',null,['class'=>'form-control' , 'maxlength'=>'10','placeholder'=>'ingrese el número de teléfono movil' , 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo opcional'])!!}

		</div>

	</div>


	



<div class="col-lg-6"> 

		<div class="form-group tooltip-demo has-success"> 
			<b>{!!Form::label('EMAIL')!!}</b>	 	<font style="color:red">*</font>

			{!!Form::text('email',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Email del taller', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo obligatorio'])!!}
		</div>

	</div>


	<div class="col-lg-3"> 

		<div class="form-group tooltip-demo"> 
			<b>{!!Form::label('PÁGINA WEB')!!}</b>

			{!!Form::text('pagina_web',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'www.tallerDomain.com', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo opcional'])!!}
		</div>

	</div>

	<div class="col-lg-3"> 

		<div class="form-group tooltip-demo"> 
			<b>{!!Form::label('FACEBOOK')!!}</b>

			{!!Form::text('facebook',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'www.facebook.com/taller', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo opcional'])!!}
		</div>

	</div>


	<div class="col-lg-3"> 

		<div class="form-group tooltip-demo"> 
			<b>{!!Form::label('INSTAGRAM')!!}</b>

			{!!Form::text('instagram',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'@tallerNew', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo opcional'])!!}
		</div>

	</div>


	<div class="col-lg-3"> 

		<div class="form-group tooltip-demo"> 
			<b>{!!Form::label('LINKEDIN')!!}</b>

			{!!Form::text('linkedin',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'www.linkedin.com/mitallers', 'data-toggle'=>'tooltip','data-placement'=>'top','title'=>'campo opcional'])!!}
		</div>

	</div>



