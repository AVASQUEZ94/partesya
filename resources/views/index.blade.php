@extends('layout.layout')

@section('content')

<div style="margin:0 auto; width:40%; margin-top:10%">


<div class="panel panel-info">

	  <div class="panel-heading">

	    <h3 class="panel-title">LOGIN</h3>

	  </div>

	  <div class="panel-body">
	  		@if(Session::has('alert'))
	<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{{Session::get('alert')}}
		</div>
	@endif

	@if(Session::has('message-error'))

<div class="alert alert-warning alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     {{Session::get('message-error')}}
      </div>

	@endif
<div class="col-sm-12">
<div class="col-sm-6">
	<center>
	    	<img src="{{asset('img/partesyalogo.jpg')}}" width="100%">
	    </center>
</div>
	<div class="col-sm-6">
	<br>
		<form action="{{asset('login')}}" method="post">
		<input type="hidden" name="_token" value="{{csrf_token()}}"></input>
			<div class="form-group">

				<label>Usuario (Email)</label>
				<input type="text" class="form-control" name="username" placeholder="Nombre de usuario">

			</div>

			<div class="form-group">

				<label>Contraseña</label>
				<input type="password" class="form-control" name="password" placeholder="Contraseña">

			</div>
			<div class="form-group">
			<a href="{{asset('recuperarclave')}}">¿Olvidaste tu contraseña?</a>
			</div>

			<div class="form-group">
			
				<input type="submit" name="" class="btn btn-info" value="LOGIN">
			
			</div>

		</form>

</div>



	
</div>


	  </div>

</div>



</div>

@stop