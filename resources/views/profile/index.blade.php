@extends('layout_sign.layout_principal_admin')

@section('page')

<div class="col-sm-12">

		<div class="col-md-1"></div>

	            <div class="col-md-10">
	          <!-- Widget: user widget style 1 -->
	          <div class="box box-widget widget-user">
	            <!-- Add the bg color to the header using any of the bg-* classes -->
	            <div class="widget-user-header bg-aqua-active" style="background: url({{asset('Adminlte/dist/img/photo1.png')}}) center center;">
	              <h3 class="widget-user-username">Elizabeth Pierce</h3>
	              <h5 class="widget-user-desc">Web Designer</h5>
	            </div>
	            <div class="widget-user-image">
	              <img class="img-circle" src="{{asset('Adminlte/dist/img/user3-128x128.jpg')}}" alt="User Avatar">
	            </div>
	            <div class="box-footer">
	              <div class="row">
	                <div class="col-sm-4 border-right">
	                  <div class="description-block">
	                    <h5 class="description-header">3,200</h5>
	                    <span class="description-text">VENTAS</span>
	                  </div>
	                  <!-- /.description-block -->
	                </div>
	                <!-- /.col -->
	                <div class="col-sm-4 border-right">
	                  <div class="description-block">
	                    <h5 class="description-header">13,000</h5>
	                    <span class="description-text">SEGUIDORES</span>
	                  </div>
	                  <!-- /.description-block -->
	                </div>
	                <!-- /.col -->
	                <div class="col-sm-4">
	                  <div class="description-block">
	                    <h5 class="description-header">35</h5>
	                    <span class="description-text">PRODUCTOS</span>
	                  </div>
	                  <!-- /.description-block -->
	                </div>
	                <!-- /.col -->
	              </div>
	              <!-- /.row -->
	            </div>
	          </div>
	          <!-- /.widget-user -->
	        </div>
</div>




<div class="col-sm-12">
	<div class="col-md-1"></div>
	<div class="col-md-10">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Perfil</a></li>
              <li><a href="#tab_2" data-toggle="tab">Privacidad y Configuraciones</a></li>   
              <li><a href="#tab_4" data-toggle="tab">Contraseña</a></li>
            
              
            </ul>
            <div class="tab-content">
             
              <div class="tab-pane active" id="tab_1">

              	  	
              	  	<div class="box box-primary">
			            <div class="box-header with-border">
			              <h3 class="box-title">Mi Información</h3>
			            </div>
			            <!-- /.box-header -->
			            <!-- form start -->
			            <form role="form">
			              <div class="box-body">

				              <div class="col-sm-4">
				              		
				              	<div class="form-group">
				                  <label for="nombres">Nombres *</label>
				                  <input type="text" class="form-control" id="nombres">
				                </div>

				               </div>



							<div class="col-sm-4">

				               <div class="form-group">
				                  <label for="Apellidos">Apellidos *</label> 
				                  <input type="text" class="form-control" id="Apellidos">
				                </div>

				             </div>


				             <div class="col-sm-4">
				               <div class="form-group">
				                  <label for="exampleInputEmail1">Email address *</label> 
				                  <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
				                </div>
				             </div>

				             <div class="col-sm-4">
				                <div class="form-group">
				                  <label for="exampleInputPassword1">N° Cuenta Bancaria *</label>
				                  <input type="text" class="form-control" id="exampleInputPassword1" >
				                </div>
				             </div>


				             <div class="col-sm-4">

				               <div class="form-group">
				                  <label for="Apellidos">Nombres del propietario *</label> 
				                  <input type="text" class="form-control" id="Propietario">
				                </div>

				             </div>

				              <div class="col-sm-4">

				               <div class="form-group">
				                  <label for="Apellidos">Nombre del Establecimiento *</label> 
				                  <input type="text" class="form-control" id="Propietario">
				                </div>

				             </div>

				            

				             <div class="col-sm-4">

				               <div class="form-group">
				                  <label for="numero">Número de contacto *</label> 
				                  <input type="text" class="form-control" id="Propietario" name="numero">
				                </div>

				             </div>

				                <div class="col-sm-4">

				               <div class="form-group">
				                  <label for="Dirección">Dirección de establecimiento *</label> 
				                 <textarea class="form-control" name="direccion"></textarea>
				                </div>

				             </div>

				                   <div class="col-sm-4">

				               <div class="form-group">
				                  <label for="Apellidos">Cuenta con Sucursales *</label> <br>
				                <input type="radio" name="sucurs" > Si <br>
				                <input type="radio" name="sucurs" checked> No

				                </div>

				             </div>


				         <div class="col-sm-12"><strong>Nota:</strong> Los campos con * son requeridos.</div>

				           
				              <div class="box-footer">
				               <br>
				                <button type="submit" class="btn btn-primary">Guardar Nuevos Cambios</button>
				              </div>
			              

			              </div>
			              <!-- /.box-body -->


			            </form>
			          </div>

              	  

              </div>

              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">


              		<div class="box box-primary">
			            <div class="box-header with-border">
			              <h3 class="box-title">Privacidad</h3>
			            </div>
			            <!-- /.box-header -->
			            <!-- form start -->
			            <form role="form">
			              <div class="box-body">

				       

			            <section class="col-lg-6">
             		
             			   <input type="checkbox" name="p_todos" checked> Todos pueden encontrarme <br>

              			  <input type="checkbox" name="p_notificiar" checked> Notificar publicaciones a Todos <br>

             			 <input type="checkbox" name="p_ver" checked> Todos pueden ver el precio de las publicaciones  <br>



             			</section>
            



             			<section class="col-lg-6">

             			<!--
             			<div class="col-sm-12">
             				<label>Imagen de Fondo * </label>
             			  <img src="{{asset('Adminlte/dist/img/photo1.png')}}" width="100%" height="200px" alt="Imagen no encontrada">
             		  </div>


             			  <br>

             			  <div class="col-sm-12">
             			  <label>Imagen de Perfil * </label>
             			  <img src="{{asset('Adminlte/dist/img/user3-128x128.jpg')}}" height="200px" width="60%">
             			  </div>
								-->
             			</section>


			              </div>
			              <!-- /.box-body -->


			            </form>
			         
			        </div>



             	
             

            

              </div>
              
                <div class="tab-pane" id="tab_4">
               
                	<div class="box box-primary">
			            <div class="box-header with-border">
			              <h3 class="box-title">Cambiar Contraseña </h3>
			            </div>
			            <!-- /.box-header -->
			            <!-- form start -->
			            <form role="form">
			              <div class="box-body">

				             <div class="col-sm-3">
				                <div class="form-group">
				                  <label for="exampleInputPassword1"><i class="fa fa-key"></i> Contraseña *</label> 
				                  <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña" value="" autocomplete="off">
				                </div>
				             </div>

				               <div class="col-sm-3">
				                <div class="form-group">
				                  <label for="exampleInputPassword12"> <i class="fa fa-key"></i> Repetir Contraseña * </label> 
				                  <input type="password" class="form-control" id="exampleInputPassword12" placeholder="Contraseña" value="" autocomplete="off">
				                </div>
				             </div>


				              

			              </div>
			              <!-- /.box-body -->

			              <div class="box-footer">
			                <button type="submit" class="btn btn-primary">Guardar Nueva Contraseña</button>
			              </div>
			            </form>
			          </div>



              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->

</div>


@stop