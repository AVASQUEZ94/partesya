-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-10-2016 a las 22:48:00
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `androidbd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `almacen`
--

DROP TABLE IF EXISTS `almacen`;
CREATE TABLE `almacen` (
  `id_almacen` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefono_celular` varchar(20) NOT NULL,
  `telefono_fijo` varchar(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `latitud` varchar(100) NOT NULL,
  `longitud` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `almacen`
--

INSERT INTO `almacen` (`id_almacen`, `id_usuario`, `nombre`, `direccion`, `email`, `telefono_celular`, `telefono_fijo`, `status`, `latitud`, `longitud`, `created_at`, `updated_at`) VALUES
(1, 1, 'ALDA', 'Avenida caracas Gran Mansion Piso 2', 'alda@gmail.com', '+58 414 3232333', '+58 212 3232333', 1, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 'Distriparte', 'Avenida Venezuela Piso 4', 'distriparte@gmail.com', '+58 414 555533', '+58 212 3232333', 1, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, 'ECOPARTES', 'Direcion uno dos tres', 'ecopartes@gmail.com', '+58 414 555533', '+58 212 3232333', 1, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `almacen_producto`
--

DROP TABLE IF EXISTS `almacen_producto`;
CREATE TABLE `almacen_producto` (
  `id` int(11) NOT NULL,
  `id_almacen` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `precio` double NOT NULL,
  `stock` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `almacen_producto`
--

INSERT INTO `almacen_producto` (`id`, `id_almacen`, `id_producto`, `precio`, `stock`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 500, '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog`
--

DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `comentario` varchar(50) NOT NULL,
  `id_almacen` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `blog`
--

INSERT INTO `blog` (`comentario`, `id_almacen`, `created_at`, `updated_at`) VALUES
('Repuestos y partes para taxi', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('Autopartes livianos en general', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('Partes Economicas chevrolet', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carro`
--

DROP TABLE IF EXISTS `carro`;
CREATE TABLE `carro` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `modelo` varchar(100) NOT NULL,
  `marca` varchar(100) NOT NULL,
  `año` varchar(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `carro`
--

INSERT INTO `carro` (`id`, `id_usuario`, `modelo`, `marca`, `año`, `created_at`, `updated_at`) VALUES
(1, 1, 'FK', 'FORD', '2016', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `linea` varchar(100) NOT NULL,
  `modelo` varchar(100) NOT NULL,
  `marca` varchar(50) NOT NULL,
  `fabricante` varchar(50) NOT NULL,
  `proveedor` varchar(50) NOT NULL,
  `año` varchar(5) NOT NULL,
  `url_imagen` varchar(200) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `nombre`, `linea`, `modelo`, `marca`, `fabricante`, `proveedor`, `año`, `url_imagen`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Bomba de agua', 'Ford', 'FK', 'Chevrolet', 'che1', 'prin1', '2012', 'www/abcde.png', 1, '2016-10-01 19:20:35', '0000-00-00 00:00:00'),
(2, 'Volante', 'Ford', 'FK1', 'Volkswagen', 'volks1', 'pos2', '2013', 'www/abcde.png', 1, '2016-10-01 19:20:52', '0000-00-00 00:00:00'),
(3, 'Asiento', 'Ford', 'FK2', 'Honda', 'hon1', 'clar3', '2014', 'www/abcde.png', 1, '2016-10-01 19:20:58', '0000-00-00 00:00:00'),
(4, 'Caucho', 'Ford', 'FK3', 'Ford', 'ford1', 'plus4', '2015', 'www/abcde.png', 1, '2016-10-01 19:21:04', '0000-00-00 00:00:00'),
(5, 'Rin', 'Ford', 'FK4', 'Fiat', 'fiat1', 'meta5', '2016', 'www/abcde.png', 1, '2016-10-01 19:21:11', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `taller`
--

DROP TABLE IF EXISTS `taller`;
CREATE TABLE `taller` (
  `id_taller` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `movil` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `rol` int(1) DEFAULT '2',
  `miembro_hasta` date NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `address`, `movil`, `email`, `password`, `status`, `rol`, `miembro_hasta`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Anyelo Vasquez', 'Venezuela', '0581393', 'angelovasq@hotmail.com', '$2y$10$hIKLrafLIMZ0qM2jTV7mG.c9MEoxk/6qpAAvvhX3.Ssk7CxP3rjDC', 1, 1, '0000-00-00', 'GEhmgWCzbdx6m69tpKWaI9Vo9WKGFJ0UBLL1E84IL7W4lHWUtzzsJ7hErCpO', '2016-05-03 22:30:32', '2016-10-02 22:24:39'),
(13, 'jose', 'chivacoa', '123456777333', 'angelovasq222@hotmail.com', '$2y$10$rhGAHFiThss9J7X4zlXSGOp5fbX.nrOsVcOVqlhKG0QxpgEEyhFjq', 0, 2, '0000-00-00', NULL, '2016-05-05 20:36:27', '2016-05-05 20:37:01'),
(16, 'Jorge Briceño', '', '', 'jbriceno@compuzoft.com', '$2y$10$e3DPHgl1Vy6Ni2e8v/0.4OV.xvoMSAHN4snZgscISHhCNMutu/hlW', 1, 1, '0000-00-00', NULL, '2016-10-02 03:58:46', '2016-10-02 04:00:57'),
(17, 'Daniel De La Cruz', '', '', 'ddlcruz@gmail.com', '$2y$10$KZXTw4DVXwBKrOMNzfwzbuXzNAF0AEs7ZCu0C2zHlS053lU5PiZ2y', 1, 2, '0000-00-00', NULL, '2016-10-02 04:05:30', '2016-10-02 04:05:30'),
(18, 'Josue Linero', '', '3005390', 'comercial@partesya.com.co', '$2y$10$TBz0fVYGxAJdZMQ4Bt2wT.hTxhtAfRGngz10Cl7bmDWoeFdXZWYT2', 1, 2, '0000-00-00', NULL, '2016-10-02 04:06:17', '2016-10-02 04:06:41');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `almacen`
--
ALTER TABLE `almacen`
  ADD PRIMARY KEY (`id_almacen`);

--
-- Indices de la tabla `almacen_producto`
--
ALTER TABLE `almacen_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id_almacen`);

--
-- Indices de la tabla `carro`
--
ALTER TABLE `carro`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `almacen`
--
ALTER TABLE `almacen`
  MODIFY `id_almacen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `almacen_producto`
--
ALTER TABLE `almacen_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `carro`
--
ALTER TABLE `carro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
