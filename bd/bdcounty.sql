-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-08-2016 a las 23:19:31
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `county_web`
--
DROP DATABASE `county_web`;
CREATE DATABASE IF NOT EXISTS `county_web` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `county_web`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `digital_index`
--

CREATE TABLE `digital_index` (
  `id_digital_index` int(11) NOT NULL,
  `uniquekey` varchar(20) NOT NULL,
  `doc_number` varchar(20) NOT NULL,
  `grantor` varchar(100) NOT NULL,
  `grantee` varchar(100) NOT NULL,
  `reference` varchar(100) NOT NULL,
  `document_type` varchar(100) NOT NULL,
  `prior_document` varchar(100) NOT NULL,
  `book` varchar(100) NOT NULL,
  `volume` varchar(100) NOT NULL,
  `page` varchar(20) NOT NULL,
  `field_date` varchar(20) NOT NULL,
  `instrument_date` varchar(20) NOT NULL,
  `county` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `id_index` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `digital_index`
--

INSERT INTO `digital_index` (`id_digital_index`, `uniquekey`, `doc_number`, `grantor`, `grantee`, `reference`, `document_type`, `prior_document`, `book`, `volume`, `page`, `field_date`, `instrument_date`, `county`, `state`, `id_index`) VALUES
(1, 'REC09960668', '53355', 'CITIFINANCIAL INC', 'HERRERA LUIS D ET UX', '', 'MTG REL', '880/335', 'REC', '0996', '0668', '1/3/2000', '12/27/1999', 'LEA', 'OK', '1'),
(2, 'REC09960670', '53356', 'CITIFINANCIAL INC', 'BURT GREGORY D ET UX', '', 'MTG REL', '930/350', 'REC', '0996', '0670', '1/3/2000', '12/27/1999', 'LEA', 'OK', '1'),
(3, 'REC09960672', '53357', 'CITIFINANCIAL INC', 'MARTINEZ PEDRO V JR ET AL', '', 'MTG REL', '907/861', 'REC', '0996', '0672', '1/3/2000', '12/27/1999', 'LEA', 'OK', '1'),
(4, 'REC09960674', '53358', 'FIRST NATIONWIDE MTG CORP', 'CRENSHAW ED P ET UX', '', 'MTG REL', '299/605', 'REC', '0996', '0674', '1/3/2000', '10/4/1999', 'LEA', 'OK', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `index`
--

CREATE TABLE `index` (
  `id_index` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `index`
--

INSERT INTO `index` (`id_index`, `name`) VALUES
(1, 'LEA'),
(2, 'JEAN');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `digital_index`
--
ALTER TABLE `digital_index`
  ADD PRIMARY KEY (`id_digital_index`);

--
-- Indices de la tabla `index`
--
ALTER TABLE `index`
  ADD PRIMARY KEY (`id_index`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `digital_index`
--
ALTER TABLE `digital_index`
  MODIFY `id_digital_index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `index`
--
ALTER TABLE `index`
  MODIFY `id_index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
