-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-03-2017 a las 03:16:04
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `porgy_partesya`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historico_sesion`
--

CREATE TABLE IF NOT EXISTS `historico_sesion` (
`id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `address` varchar(300) DEFAULT NULL,
  `movil` varchar(50) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `rol` int(1) DEFAULT NULL,
  `google` longtext,
  `facebook` longtext NOT NULL,
  `twiter` longtext,
  `picture` longtext,
  `displayname` longtext,
  `miembro_hasta` date NOT NULL,
  `tipo_transaccion` varchar(100) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `historico_sesion`
--

INSERT INTO `historico_sesion` (`id`, `id_usuario`, `nombre`, `address`, `movil`, `email`, `password`, `status`, `rol`, `google`, `facebook`, `twiter`, `picture`, `displayname`, `miembro_hasta`, `tipo_transaccion`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 2, '222', '2222', '2222', '22222@gmail.com', '$2y$10$WfViWw3l6x5QqPcg99Ogn.N.lMaWDLlmlWGK3q2RugFx4VGt.mYt6', 1, 2, NULL, '', NULL, NULL, NULL, '0000-00-00', 'Agregado', NULL, '2017-03-05 04:45:11', '2017-03-05 04:45:11'),
(2, 2, '3333', '3333', '33333', '', '$2y$10$1.UoBKadqvqUNCSICdeJ/ui4iz6E7/T9oYj9qtTEAfO9cOBwHBsaK', 1, 2, NULL, '', NULL, NULL, NULL, '0000-00-00', 'Agregado', NULL, '2017-03-06 00:01:01', '2017-03-06 00:01:01'),
(3, 50, '4444', '4444', '4444|', '', '$2y$10$JFjiTYMOP0AW8lpyS8HUDug5rx4AXUelaVjVVSQc.CxJArqbSW.5y', 1, 2, NULL, '', NULL, NULL, NULL, '0000-00-00', 'Agregado', NULL, '2017-03-06 00:18:20', '2017-03-06 00:18:20'),
(4, 51, '5', '5', '5', '', '$2y$10$SyPQTtISDQoGH4VkKWoAMeImqDRLeCKIgr802iiESHkIrSX7I74nC', 1, 2, NULL, '', NULL, NULL, NULL, '0000-00-00', 'Agregado', NULL, '2017-03-06 00:22:56', '2017-03-06 00:22:56'),
(5, 52, '6', '6', '6', '6@gmail.com', '$2y$10$GMAi.BLBbwKlNgDkHuUdPOmwzcgIeMNNfMQQGogQ1GXgJY/GaaAvK', 1, 2, NULL, '', NULL, NULL, NULL, '0000-00-00', 'Agregado', NULL, '2017-03-06 00:30:46', '2017-03-06 00:30:46'),
(6, 50, '4444', NULL, NULL, 'cuatro@gmail.com', '$2y$10$B7X2YFcxPXeNDdD9QApFN.V6qaXxe1urgToeXl1Uy8dOFGnzpML36', 0, NULL, NULL, '', NULL, NULL, NULL, '0000-00-00', 'Eliminado', NULL, '2017-03-06 01:44:10', '2017-03-06 01:44:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historico_taller`
--

CREATE TABLE IF NOT EXISTS `historico_taller` (
`id` int(11) NOT NULL,
  `id_taller` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `nombre_dueno` varchar(100) NOT NULL,
  `nombre_gerente` varchar(100) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `tipo_taller` int(11) NOT NULL,
  `telefono_fijo` varchar(15) NOT NULL,
  `telefono_movil` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pagina_web` varchar(100) NOT NULL,
  `latitud` varchar(100) NOT NULL,
  `longitud` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `tipo_transaccion` varchar(100) NOT NULL,
  `id_creador` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `historico_taller`
--

INSERT INTO `historico_taller` (`id`, `id_taller`, `nombre`, `nombre_dueno`, `nombre_gerente`, `direccion`, `tipo_taller`, `telefono_fijo`, `telefono_movil`, `email`, `pagina_web`, `latitud`, `longitud`, `status`, `tipo_transaccion`, `id_creador`, `created_at`, `updated_at`) VALUES
(9, 0, 'jose', 'josed', 'joseg', 'dredd', 1, '123456', '345678', 'vffffdghd@jkd', 'www.jose.com', '', '', 1, 'Agregado', 0, '2017-02-06 00:00:00', '2017-02-06 00:00:00'),
(10, 0, 'eeee', 'eee', 'eee', 'eeeee', 2, '3', '4', 'hhg', 'hhgfhfghfhfh', '', '', 0, 'Consultado', 0, '2017-02-26 00:00:00', '2017-02-26 00:00:00'),
(11, 0, 'dddd', 'dddd', 'dddd', 'ddddd', 2, '33223d', '2233232', '2323d23d2d', '3d232d32d3d2', '', '', 1, 'Modificado', 0, '2017-02-26 00:00:00', '2017-02-26 00:00:00'),
(12, 0, 'O', 'O', 'O', 'CHIVACOA', 1, 'O', 'O', 'o@gmail.com', 'O', '10.997901751041832', '-74.80422703750003', 0, 'Agregado', 0, '2017-02-26 22:22:01', '2017-02-26 22:22:01'),
(13, 0, 'Y', 'Y', 'Y', 'CHIVACOA', 1, 'Y', 'Y', 'YHONNYRS_95@HOTMAIL.COM', 'Y', '10.1572383', '-68.89732680000003', 0, 'Agregado', 0, '2017-02-26 22:24:55', '2017-02-26 22:24:55'),
(14, 22, 'casrwash', 'anyelo', '', 'chivacpa', 1, '', '', 'annytorin582@hotmail.com', '', '10.1572383', '-68.89732680000003', 0, 'Agregado', 2, '2017-02-26 22:57:52', '2017-02-26 22:57:52'),
(15, 23, 'CarMotors', 'Joselang', 'José Gregorio Peña', 'chivacioa', 3, '', '', 'josegp29@gmail.com', 'josegp29@gmail.com', '10.1572383', '-68.89732680000003', 0, 'Agregado', 2, '2017-02-26 23:20:37', '2017-02-26 23:20:37'),
(16, 25, 'a', 'a', 'a', 'CHIVACOA', 1, 'a', 'a', 'annytorin582@hotmail.com', 'a', '10.1572383', '-68.89732680000003', 1, 'Agregado', 2, '2017-02-27 00:59:24', '2017-02-27 00:59:24'),
(17, 26, 'yyyyyy', 'Yyyyy', 'Yyyyyy', 'lara', 4, '0251', '0416', 'jgp@yahoo.s', 'no', '10.1537842', '-69.85974060000001', 1, 'Agregado', 2, '2017-02-27 01:11:19', '2017-02-27 01:11:19'),
(18, 27, 'eeeeeeeeee', 'eeeeeee', 'eeeeeeeeee', 'CHIVACOA', 2, 'eeeeeeeee', 'eeeeeeeeee', 'eee@hotmail.com', 'dddddd', '10.1572383', '-68.89732680000003', 1, 'Agregado', 2, '2017-02-27 19:13:41', '2017-02-27 19:13:41'),
(19, 28, 'otro', 'oio', 'oio', 'CHIVACOA', 1, '135', '133', 'otro@hotmail.com', 'otro', '10.1572383', '-68.89732680000003', 1, 'Agregado', 2, '2017-02-27 21:13:09', '2017-02-27 21:13:09'),
(20, 29, 'eliminar', 'eliminar', 'eliminar', 'lara', 2, '231545', '23521', 'eli@gmail.com', 'elieli', '10.1537842', '-69.85974060000001', 1, 'Agregado', 2, '2017-02-28 12:59:50', '2017-02-28 12:59:50'),
(21, 30, 'eliminar2', 'eliminar2', 'eli2', 'lara', 1, 'eli', 'eli', 'eli@gmail.com', '2222sss', '10.1537842', '-69.85974060000001', 1, 'Agregado', 2, '2017-02-28 13:42:53', '2017-02-28 13:42:53'),
(22, 31, 'eliminar2333', 'eliminar', 'eeeeeeeeee', 'lara', 1, 'erf', 'sdxs', 'dj_leomar_mega@hotmail.com', 'dddd', '10.1537842', '-69.85974060000001', 1, 'Agregado', 2, '2017-02-28 13:44:43', '2017-02-28 13:44:43'),
(23, 32, '66', '6', '6', 'caracas', 1, '6', '6', '6@gmail.com', '6', '10.997901751041832', '-74.80422703750003', 1, 'Agregado', 2, '2017-02-28 13:53:35', '2017-02-28 13:53:35'),
(24, 33, 'blanca', 'blanca', 'blanca', 'lara', 1, 'blanca', 'blanca', 'blanca@gmail.com', 'blanca', '10.1537842', '-69.85974060000001', 1, 'Agregado', 2, '2017-02-28 14:14:53', '2017-02-28 14:14:53'),
(25, 34, 'blanca', 'blanca', 'blanca', 'lara', 1, 'blanca', '2566', 'blanca@gmail.com', 'nb', '10.997901751041832', '-74.80422703750003', 1, 'Agregado', 2, '2017-02-28 14:17:32', '2017-02-28 14:17:32'),
(26, 35, 'qqqq', 'qqqq', 'qqqq', 'san antonio', 1, '2222', '2222', '2222@gmail.com', '22222', '29.4241219', '-98.49362819999999', 1, 'Agregado', 2, '2017-02-28 15:03:53', '2017-02-28 15:03:53'),
(27, 36, 'PRUEBA', 'prueba', 'prieba', 'lara', 1, '355511', '51151', 'prue@gmail.com', 'jhdavhdvsa ', '10.1537842', '-69.85974060000001', 1, 'Agregado', 2, '2017-02-28 17:12:51', '2017-02-28 17:12:51'),
(28, 36, 'PRUEBA', 'prueba', 'prieba', 'lara', 1, '355511', '51151', 'prue@gmail.com', 'jhdavhdvsa ', '10.1537842', '-69.85974060000001', 1, 'Eliminado', 2, '2017-02-28 17:13:42', '2017-02-28 17:13:42'),
(29, 36, 'PRUEBA', 'prueba', 'prieba', 'lara', 1, '355511', '51151', 'prue@gmail.com', 'jhdavhdvsa ', '10.1537842', '-69.85974060000001', 1, 'Eliminado', 2, '2017-02-28 17:16:37', '2017-02-28 17:16:37'),
(30, 37, 'TTTTTTT', 'TTTTT', 'TTTTT', 'LORO', 1, 'TTTTT', 'TTTTT', 'TTTTT@GMAIL.COM', 'TTTT', '22.7602827', '84.15142800000001', 1, 'Agregado', 2, '2017-02-28 17:21:48', '2017-02-28 17:21:48'),
(31, 37, 'TTTTTTT', 'TTTTT', 'TTTTT', 'LORO', 1, 'TTTTT', 'TTTTT', 'TTTTT@GMAIL.COM', 'TTTT', '22.7602827', '84.15142800000001', 1, 'Eliminado', 2, '2017-02-28 17:22:46', '2017-02-28 17:22:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historico_taller_servicio`
--

CREATE TABLE IF NOT EXISTS `historico_taller_servicio` (
`id` int(11) NOT NULL,
  `id_taller` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `id_historico_taller` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `historico_taller_servicio`
--

INSERT INTO `historico_taller_servicio` (`id`, `id_taller`, `id_servicio`, `id_historico_taller`, `created_at`, `updated_at`) VALUES
(1, 23, 4, 15, '2017-02-26', '2017-02-26'),
(2, 23, 1, 15, '2017-02-26', '2017-02-26'),
(3, 23, 3, 15, '2017-02-26', '2017-02-26'),
(4, 23, 5, 15, '2017-02-26', '2017-02-26'),
(5, 26, 4, 17, '2017-02-27', '2017-02-27'),
(6, 26, 3, 17, '2017-02-27', '2017-02-27'),
(7, 26, 1, 17, '2017-02-27', '2017-02-27'),
(8, 26, 5, 17, '2017-02-27', '2017-02-27'),
(9, 29, 2, 20, '2017-02-28', '2017-02-28'),
(10, 29, 4, 20, '2017-02-28', '2017-02-28'),
(11, 29, 1, 20, '2017-02-28', '2017-02-28'),
(12, 33, 3, 24, '2017-02-28', '2017-02-28'),
(13, 33, 4, 24, '2017-02-28', '2017-02-28'),
(14, 34, 4, 25, '2017-02-28', '2017-02-28'),
(15, 36, 4, 27, '2017-02-28', '2017-02-28'),
(16, 36, 1, 27, '2017-02-28', '2017-02-28');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `historico_sesion`
--
ALTER TABLE `historico_sesion`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `historico_taller`
--
ALTER TABLE `historico_taller`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `historico_taller_servicio`
--
ALTER TABLE `historico_taller_servicio`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `historico_sesion`
--
ALTER TABLE `historico_sesion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `historico_taller`
--
ALTER TABLE `historico_taller`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `historico_taller_servicio`
--
ALTER TABLE `historico_taller_servicio`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
