-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-02-2017 a las 01:16:48
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `porgy_partesya`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_reset_apk`
--

CREATE TABLE `password_reset_apk` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `password_reset_apk`
--

INSERT INTO `password_reset_apk` (`id`, `email`, `codigo`, `estado`, `created_at`) VALUES
(5, 'sugey.useche@gmail.com', 'c19751', 0, '0000-00-00 00:00:00'),
(6, 'nbriceno@compuzoft.com', 'b7fbaa', 0, '0000-00-00 00:00:00'),
(7, 'ingdelacruzariza@gmail.com', '0aa4f9', 0, '0000-00-00 00:00:00'),
(8, 'angelovasq@hotmail.com', 'c78105', 0, '0000-00-00 00:00:00'),
(9, 'jbriceno@compuzoft.com', 'f89c30', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`id`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Mecánica especializada Renault', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Limpieza de inyectores', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Alineación', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Balanceo', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `taller`
--

CREATE TABLE `taller` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `nombre_dueno` varchar(100) NOT NULL,
  `nombre_gerente` varchar(100) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `tipo_taller` int(11) NOT NULL,
  `telefono_fijo` varchar(15) NOT NULL,
  `telefono_movil` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pagina_web` varchar(100) NOT NULL,
  `latitud` varchar(100) NOT NULL,
  `longitud` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `taller`
--

INSERT INTO `taller` (`id`, `nombre`, `nombre_dueno`, `nombre_gerente`, `direccion`, `tipo_taller`, `telefono_fijo`, `telefono_movil`, `email`, `pagina_web`, `latitud`, `longitud`, `status`, `created_at`, `updated_at`) VALUES
(1, 'prueba', 'bill', '', 'colombia city', 1, '', '', 'colombiaprueba@gmail.com', '', '', '', 1, '2017-02-13 00:40:17', '2017-02-13 02:00:10'),
(2, 'taller 2', 'mart', '', 'barranquilla', 3, '', '', 'crismart@gmail.com', '', '4.7033572105477', '-74.13576596679684', 1, '2017-02-13 00:47:47', '2017-02-16 23:38:33'),
(3, 'tallerindolenciaa', 'cristina', 'ddddd', 'colombia', 3, '12312', '1234', 'angelovasq@hotmail.com', 'abcess.com', '27.839076094777816', '2.98828125', 1, '2017-02-13 01:30:05', '2017-02-16 23:43:37'),
(4, 'aeropuerto motors', 'sebastian', '', 'aeropuerto equis', 1, '', '', 'aeropuero@gmail.com', '', '4.703464137615548', '-74.13576596679684', 1, '2017-02-16 01:47:32', '2017-02-16 01:47:32'),
(5, 'aeropuerto motors', 'sebastian', '', 'aeropuerto equis', 1, '', '', 'aeropuero@gmail.com', '', '4.7033572105477', '-74.13374894561764', 1, '2017-02-16 01:48:33', '2017-02-16 01:48:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `taller_servicio`
--

CREATE TABLE `taller_servicio` (
  `id` int(11) NOT NULL,
  `id_taller` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `taller_servicio`
--

INSERT INTO `taller_servicio` (`id`, `id_taller`, `id_servicio`, `created_at`, `updated_at`) VALUES
(14, 2, 2, '2017-02-16 23:38:33', '2017-02-16 23:38:33'),
(15, 3, 4, '2017-02-16 23:43:38', '2017-02-16 23:43:38'),
(16, 3, 3, '2017-02-16 23:43:38', '2017-02-16 23:43:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_taller`
--

CREATE TABLE `tipo_taller` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_taller`
--

INSERT INTO `tipo_taller` (`id`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Carrocería', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Eléctrico', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Llantas', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Motores', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `movil` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `rol` int(1) DEFAULT '2',
  `google` longtext COLLATE utf8_unicode_ci,
  `facebook` longtext COLLATE utf8_unicode_ci,
  `twitter` longtext COLLATE utf8_unicode_ci,
  `picture` longtext COLLATE utf8_unicode_ci,
  `DisplayName` longtext COLLATE utf8_unicode_ci,
  `miembro_hasta` date NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `address`, `movil`, `email`, `password`, `status`, `rol`, `google`, `facebook`, `twitter`, `picture`, `DisplayName`, `miembro_hasta`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Anyelo Vasquez', 'Venezuela', '0581393', 'angelovasq@hotmail.com', '$2y$10$t7RJS.FrkBjqDHCzPYiIDOJh6YK3FbARdeEC2XssAN7XS5It.wSoq', 1, 1, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlhdCI6MTQ4MDQ2MjU0OCwiZXhwIjoxNDgxNjcyMTQ4fQ.-5FL', '2016-05-03 22:30:32', '2016-11-30 03:35:48'),
(13, 'jose', 'chivacoa', '123456777333', 'angelovasq222@hotmail.com', '$2y$10$rhGAHFiThss9J7X4zlXSGOp5fbX.nrOsVcOVqlhKG0QxpgEEyhFjq', 0, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2016-05-05 20:36:27', '2016-05-05 20:37:01'),
(16, 'Jorge Briceño', '', '', 'jbriceno@compuzoft.com', '$2y$10$BtsUuxIk8fX8nT/wso277O6YKq8VjyvOy7GqdDmgnn4VaaLjCfxoe', 1, 1, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE2LCJpYXQiOjE0ODA1MjczMzksImV4cCI6MTQ4MTczNjkzOX0.x_N', '2016-10-02 03:58:46', '2016-11-30 21:35:39'),
(17, 'Daniel De La Cruz', '', '', 'ddlcruz@gmail.com', '$2y$10$jXkOmMDuAMObUxzp8Ytun.ZzaW50aChElwEPmP9WcnfFO0xx9/XhK', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'Vxqjlb9xATkRPW5HhHlPcwyF1Idgtcsbtj00WdZPQcgPXPjsT8P7mnF4Kejh', '2016-10-02 04:05:30', '2016-10-08 00:51:33'),
(18, 'Josue Linero', '', '3005390', 'comercial@partesya.com.co', '$2y$10$TBz0fVYGxAJdZMQ4Bt2wT.hTxhtAfRGngz10Cl7bmDWoeFdXZWYT2', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2016-10-02 04:06:17', '2016-10-02 04:06:41'),
(19, 'Sugey Useche', 'Palmira', '62727736', 'sugey.useche@gmail.com', '$2y$10$kNB3dk1TQ8bJVxUZPE5N9uv.ubjLbH0PESyNS9SWNP/twY2gXk9um', 1, 2, '115920329516106030767', '10207407014485904', NULL, 'https://lh3.googleusercontent.com/-FJIZVOUhj-E/AAAAAAAAAAI/AAAAAAAAAMA/FxrUGbTDs8Q/photo.jpg?sz=50', NULL, '0000-00-00', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE5LCJpYXQiOjE0ODI5NDYyMjgsImV4cCI6MTQ4NDE1NTgyOH0.8K2', '2016-11-19 22:05:31', '2016-12-28 21:30:28'),
(21, 'aaaaa sssss', NULL, NULL, 'xxxx@gmail.com', '$2y$10$r/mafdfPWoPfWsR8OQ1Xc.4HqaYK3AgUzx78/UvUB0t.p5UixuQlW', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2016-11-19 22:19:21', '2016-11-19 22:19:21'),
(24, 'prueba', NULL, NULL, 'pepitoperez@hotmail.com', '$2y$10$pnLpDhyH7IFTxbvjWmRaWu/GUCsEvBsiE4xn.1q0.4lK7ee67EqyS', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'DcfaCGrHOetnOPb9kCpS2ijO5I2nBiRU1PjxzMeB8d7IIkgEMLqeyLy1efek', '2016-11-20 02:28:41', '2016-11-20 02:28:41'),
(25, 'Juan Prueba', 'Los naranjos', NULL, 'pepitoperez2@hotmail.com', '$2y$10$w31LhIdVTt51.Hlc1hMck.LQMxfO8vW.JB07wbhGTKdpSxUTzEozm', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjI1LCJpYXQiOjE0ODA0MTkzMDQsImV4cCI6MTQ4MTYyODkwNH0.Ncj', '2016-11-20 02:42:05', '2016-11-29 15:35:04'),
(26, 'Prueba uno', NULL, NULL, 'pruebay@hotmail.com', '$2y$10$R0qLRvQRsNqATTqEi9h4Pudd/4Kxv8o73kCTeV1xdHbg97b88CaZq', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'miGcOWFHSTeGTcxuzIOwTIn5nNQzEmgd57BaAdmYmfg8juWY6z42m0pHrtvT', '2016-11-22 09:44:26', '2016-11-22 09:44:26'),
(27, '', 'Castor av', '78855783', 'probando@gmail.com', '$2y$10$d5HJYUvW.V26nKmvaVChG.C0zZw4ep4gRnc/c22lCpugmrMxclc3u', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'GYcB5L4ZECV7CLaTTfUlkHna5KNBrpmBChqAeWcgIN7dkMfnqY1R5mKAn9PM', '2016-11-22 09:52:12', '2016-11-24 17:52:39'),
(28, 'Nadya Briceño', NULL, NULL, 'nbriceno@compuzoft.com', '$2y$10$JlDM7VoT/UMwvMeyuQlxtO/dm78NMjYShPSnJUqZhvLcmI4QmJB6O', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjI4LCJpYXQiOjE0ODMwMjk0MDgsImV4cCI6MTQ4NDIzOTAwOH0.w2w', '2016-11-23 00:52:31', '2016-12-29 20:36:48'),
(29, 'Félix castro', NULL, NULL, 'felixcastro1993@gmail.com', '$2y$10$DIDxU/YmJ7tXMDwqA0NS1u8J28zvgHwpo7KRZKPEFyCn0.uyx7hn6', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'npo7uITaj72nwodeB6PvBvJnMfclyWSHcYYnUxCpLwknkXg2lYu3g57r4edF', '2016-11-23 01:45:05', '2016-11-23 01:45:05'),
(32, 'Daniel De La Cruz Ariza', 'Carrera 42 80B-51', '3152423207', 'ingdelacruzariza@gmail.com', '$2y$10$2OdOeMFTPKwfi8JmUjka2eAwo.oVTq3JNa9RfYmD9X7aFQadb8zn.', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyLCJpYXQiOjE0ODMwMjU5NDAsImV4cCI6MTQ4NDIzNTU0MH0.ahW', '2016-11-26 18:21:58', '2016-12-29 19:39:52'),
(33, 'Roustita', NULL, NULL, '@Roustita', '', 1, 2, NULL, NULL, '64622174', NULL, NULL, '0000-00-00', NULL, '2016-11-28 03:51:13', '2016-11-28 03:51:13'),
(34, 'prueba', NULL, NULL, 'prrueba3@hotmail.com', '$2y$10$792VGLcFkSCzaXAJeP3bYuzsLeU2MsGE5ada6dgRIaXwncDzSf6Ly', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2016-11-28 06:38:02', '2016-11-28 06:38:02'),
(35, 'prueba', NULL, NULL, 'prueba3@hotmail.com', '$2y$10$x6LRVaqsDJARXqPLaeQ0dea4s0Ayo6SEdHuUv1JXJiaZgbr2/wgpa', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjM1LCJpYXQiOjE0ODAzMDA4OTgsImV4cCI6MTQ4MTUxMDQ5OH0.DF2', '2016-11-28 06:41:38', '2016-11-28 06:41:38'),
(36, 'Karen de la cruz', NULL, NULL, 'karencitadelacruz@hotmail.com', '$2y$10$RTTi.BT4neCcRbU2hvwdYuKQ9wz22ebREAKK0pTFtaZjXxBgMyhNq', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjM2LCJpYXQiOjE0ODE5OTA5MzIsImV4cCI6MTQ4MzIwMDUzMn0.6DS', '2016-12-17 20:08:52', '2016-12-17 20:08:52'),
(37, 'Daisy De La Cruz', NULL, NULL, 'daisydelacruz22@hotmail.com', '$2y$10$tYnoLuf/cWeQdSz9l3.gBun1VhQgeEEkAm0J/nuHppugj5KT.6LqO', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjM3LCJpYXQiOjE0ODIzNTY0ODMsImV4cCI6MTQ4MzU2NjA4M30.pJo', '2016-12-22 01:41:23', '2016-12-22 01:41:23'),
(38, 'Kevin Castillo', NULL, NULL, 'kevin.develop@gmail.com', '$2y$10$8ODjGb0mcsGnj4qdOgMPCOSc0HweSiujxxh0m5/NdYagSyNh76lrK', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjM4LCJpYXQiOjE0ODI5NTUxNzAsImV4cCI6MTQ4NDE2NDc3MH0.igL', '2016-12-22 23:51:36', '2016-12-28 23:59:30'),
(39, 'Carrito', NULL, NULL, 'pruebacarrito@prueba.com', '$2y$10$eUpJ00bFbJQYf1vwufqaLuJdq6ItsxVzND/ZnromtCycJkcRmR49G', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjM5LCJpYXQiOjE0ODI5NDM0ODUsImV4cCI6MTQ4NDE1MzA4NX0.hpO', '2016-12-28 20:42:41', '2016-12-28 20:44:45'),
(40, 'Carrito 2', NULL, NULL, 'pruebaCarrito2@prueba.com', '$2y$10$D.DZ6scm/daUDrYOirUuiumWLWXkBSte3qjoI85hKs/Ny8OoXQ2k2', 1, 2, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQwLCJpYXQiOjE0ODI5NDQyODUsImV4cCI6MTQ4NDE1Mzg4NX0.UMX', '2016-12-28 20:58:05', '2016-12-28 20:58:05');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `password_reset_apk`
--
ALTER TABLE `password_reset_apk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`codigo`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `taller`
--
ALTER TABLE `taller`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `taller_servicio`
--
ALTER TABLE `taller_servicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_taller`
--
ALTER TABLE `tipo_taller`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `password_reset_apk`
--
ALTER TABLE `password_reset_apk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `taller`
--
ALTER TABLE `taller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `taller_servicio`
--
ALTER TABLE `taller_servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `tipo_taller`
--
ALTER TABLE `tipo_taller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
